module gitlab.com/my-group322/pictures/pics-svc

go 1.16

require (
	github.com/alicebob/miniredis/v2 v2.16.1
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/assembla/cony v0.3.2
	github.com/aws/aws-sdk-go v1.42.18
	github.com/aws/aws-sdk-go-v2/credentials v1.6.4
	github.com/aws/aws-sdk-go-v2/service/sqs v1.13.1
	github.com/bozd4g/go-http-client v0.1.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/go-redis/redis/v8 v8.11.4
	github.com/gobwas/ws v1.1.0
	github.com/google/uuid v1.3.0
	github.com/gorilla/schema v1.2.0
	github.com/ilyasiv2003/gosqs v0.0.0-20211207224948-cd668ae9cf65
	github.com/ilyasiv2003/imgediting v0.0.0-20211206164413-c599579bdf79
	github.com/ilyasiv2003/newrelic-context v0.0.0-20211203210028-6b947dffac58
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lib/pq v1.10.4
	github.com/mattn/go-sqlite3 v2.0.1+incompatible // indirect
	github.com/newrelic/go-agent v3.15.1+incompatible
	github.com/pkg/errors v0.9.1
	github.com/rubenv/sql-migrate v0.0.0-20211023115951-9f02b1e13857
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	gitlab.com/my-group322/json-api-shared v0.0.0-20211208100130-285519047ba1
	gitlab.com/my-group322/pictures/auth-svc v0.0.0-20211206163345-bc6825367fc4
	gitlab.com/my-group322/pictures/img-moderation-lambdas/lambda/moderator v0.0.0-20211206230852-8d1ffc3392ef
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.4
)
