package fakes

import (
	"os"
	"time"
)

// FakeFileInfo
// We are mocking FileInfo interface so that we can zip files without need
// to actually work with files
type FakeFileInfo struct {
	name             string
	mode             os.FileMode
	modificationTime time.Time
}

func NewFakeFileInfo(name string, modTime time.Time) FakeFileInfo {
	return FakeFileInfo{
		name:             name,
		mode:             os.ModePerm,
		modificationTime: modTime,
	}
}

func (f FakeFileInfo) Name() string {
	return f.name
}

func (f FakeFileInfo) Size() int64 {
	return 0
}

func (f FakeFileInfo) Mode() os.FileMode {
	return f.mode
}

func (f FakeFileInfo) ModTime() time.Time {
	return f.modificationTime
}

func (f FakeFileInfo) IsDir() bool {
	return false
}

func (f FakeFileInfo) Sys() interface{} {
	return nil
}
