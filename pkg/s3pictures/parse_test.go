package s3pictures

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParsePictureKeyToID(t *testing.T) {
	t.Run("positive", func(t *testing.T) {
		id, err := ParsePictureKeyToID("pics/4.jpg")
		require.NoError(t, err)
		require.Equal(t, int64(4), id)
	})

	t.Run("negative_wrong_format", func(t *testing.T) {
		_, err := ParsePictureKeyToID("4.jpg")
		require.Error(t, err)
	})

	t.Run("negative_not_number", func(t *testing.T) {
		_, err := ParsePictureKeyToID("pics/not-number.jpg")
		require.Error(t, err)
	})
}
