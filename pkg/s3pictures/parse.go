package s3pictures

import (
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

func ParsePictureKeyToID(key string) (int64, error) {
	// some-path/3.jpg
	//           ^ID
	lastIdx := strings.LastIndex(key, "/")
	if lastIdx == -1 {
		return 0, errors.New("last slash in key not found")
	}

	key = strings.TrimSuffix(key, ".jpg")
	key = key[lastIdx+1:]

	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return 0, errors.Wrap(err, "failed to parse key")
	}

	return id, nil
}
