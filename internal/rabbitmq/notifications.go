package rabbitmq

import (
	"github.com/assembla/cony"
	"github.com/sirupsen/logrus"
)

func Notifications(rabbitMQUrl string, log *logrus.Logger) *cony.Publisher {
	cli := cony.NewClient(
		cony.URL(rabbitMQUrl),
		cony.Backoff(cony.DefaultBackoff),
	)

	exc := cony.Exchange{
		Name:       "notifications",
		Kind:       "fanout",
		AutoDelete: true,
	}
	cli.Declare([]cony.Declaration{
		cony.DeclareExchange(exc),
	})

	pbl := cony.NewPublisher(exc.Name, "")
	cli.Publish(pbl)

	go func() {
		for cli.Loop() {
			if err := <-cli.Errors(); true {
				log.Error(err)
			}
		}
	}()

	return pbl
}
