package config

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

type Config struct {
	Port                      string `required:"true" envconfig:"PORT"`
	DBUrl                     string `required:"true" envconfig:"DB_URL"`
	RabbitMQUrl               string `required:"true" envconfig:"RABBIT_MQ_URL"`
	RedisURL                  string `required:"true" envconfig:"REDIS_URL"`
	AwsRegion                 string `required:"true" envconfig:"AWS_REGION"`
	AwsAccessKeyID            string `required:"true" envconfig:"AWS_ACCESS_KEY_ID"`
	AwsSecretKey              string `required:"true" envconfig:"AWS_SECRET_KEY"`
	AwsAccountID              string `required:"true" envconfig:"AWS_ACCOUNT_ID"`
	AwsS3BucketName           string `required:"true" envconfig:"AWS_S3_BUCKET_NAME"`
	JwtSecret                 string `envconfig:"JWT_SECRET"`
	InternalAccessKey         string `envconfig:"INTERNAL_ACCESS_KEY"`
	NewRelicLicenceKey        string `envconfig:"NEW_RELIC_LICENSE_KEY"`
	ImageModerationQueue      string `required:"true" envconfig:"IMAGE_MODERATION_QUEUE"`
	IsImageModerationDisabled bool   `envconfig:"IS_IMAGE_MODERATION_DISABLED"`
}

func New(log *logrus.Logger) (*Config, error) {
	var config Config

	if err := envconfig.Process("", &config); err != nil {
		return nil, err
	}

	if config.JwtSecret == "" {
		log.Warn("You are starting with empty jwt-secret value")
	}

	return &config, nil
}
