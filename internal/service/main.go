package service

import (
	"context"
	"net/http"

	"github.com/ilyasiv2003/gosqs"
	"github.com/pkg/errors"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
	myhttp "gitlab.com/my-group322/pictures/pics-svc/internal/service/http"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/chat"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/server"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/sqs"
)

type Service struct {
	app *app.App
}

func NewService(app *app.App) *Service {
	return &Service{app}
}

func (s *Service) Run(ctx context.Context) error {
	a := s.app

	newAPI := api.New(a)

	newServer := server.New(newAPI, a.Log)
	sqsHandlersManager := sqs.NewHandlersManager(a.Log, newAPI)
	chatClient := chat.NewHandler(a.Redis, a.Log, newAPI)

	if !a.Cfg.IsImageModerationDisabled {
		err := a.Sqs.StartConsumers(map[string]gosqs.Handler{
			a.Cfg.ImageModerationQueue: sqsHandlersManager.ImageModerationResultsConsumer,
		})
		if err != nil {
			return errors.Wrap(err, "failed to start consumers")
		}
	}

	r := myhttp.Router(a, newServer, chatClient)

	listener := &http.Server{Addr: "0.0.0.0:" + a.Cfg.Port, Handler: r}

	errChan := make(chan error)
	go func() {
		if err := listener.ListenAndServe(); err != nil {
			errChan <- errors.Wrap(err, "router crashed")
		}
	}()

	select {
	case err := <-errChan:
		return err
	case <-ctx.Done():
		if err := listener.Shutdown(context.Background()); err != nil {
			return err
		}
		return nil
	}
}
