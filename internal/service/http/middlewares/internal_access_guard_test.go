package middlewares

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/stretchr/testify/require"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/my-group322/pictures/pics-svc/internal/config"
)

func TestInternalAccessGuardMiddleware(t *testing.T) {
	a := &app.App{
		Log: logrus.New(),
		Cfg: &config.Config{
			InternalAccessKey: uuid.NewString(),
		},
	}

	req, err := http.NewRequest(http.MethodGet, "users/1", nil)
	require.NoError(t, err)

	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusUnavailableForLegalReasons)
	})

	t.Run("positive", func(t *testing.T) {
		response := httptest.NewRecorder()
		req.Header.Set("Authorization", a.Cfg.InternalAccessKey)

		InternalAccessGuardMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusUnavailableForLegalReasons, response.Code)
	})

	t.Run("negative_invalid_secret", func(t *testing.T) {
		response := httptest.NewRecorder()
		req.Header.Set("Authorization", uuid.NewString())

		InternalAccessGuardMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusForbidden, response.Code)
	})
}
