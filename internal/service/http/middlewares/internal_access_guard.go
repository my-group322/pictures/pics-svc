package middlewares

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
)

func InternalAccessGuardMiddleware(app *app.App) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			key := r.Header.Get("Authorization")
			if key != app.Cfg.InternalAccessKey {
				app.Log.Error("invalid access key " + key)
				renderer.RenderStatus(w, http.StatusForbidden)
				return
			}

			next.ServeHTTP(w, r)
		})
	}
}
