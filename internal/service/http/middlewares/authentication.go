package middlewares

import (
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/dgrijalva/jwt-go"
	jwt2 "gitlab.com/my-group322/pictures/auth-svc/pkg/jwt"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
)

func AuthenticationMiddleware(app *app.App) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			key := r.Header.Get("Authorization")
			if key == app.Cfg.InternalAccessKey {
				next.ServeHTTP(w, r)
				return
			}

			log := app.Log

			authCookie, err := r.Cookie("jwt-auth")
			if err != nil {
				log.WithError(err).Error("cookie wasn't found")
				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			token, err := jwt.ParseWithClaims(authCookie.Value, &jwt2.CustomClaimsJwt{}, func(t *jwt.Token) (interface{}, error) {
				return []byte(app.Cfg.JwtSecret), nil
			})
			if err != nil {
				log.WithError(err).Error("token didn't pass verification")
				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			claims := token.Claims.(*jwt2.CustomClaimsJwt)

			ctx := r.Context()
			ctx = ctx2.RequesterToCtx(claims.UserID)(ctx)

			redisQ := app.RedisQ.Clone(ctx)

			sessionValid, err := redisQ.IsSessionPresent(claims.SessionID)
			if err != nil {
				log.WithError(err).Error("failed to check session id")
				renderer.RenderStatus(w, http.StatusInternalServerError)
				return
			}

			if !sessionValid {
				log.Error("session not valid")
				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			identity, err := redisQ.GetIdentity(claims.UserID)
			if err != nil {
				log.WithError(err).Error("failed to get last pass change")
				renderer.RenderStatus(w, http.StatusInternalServerError)
				return
			}

			if identity == nil {
				log.WithError(err).Error("identity not found")
				renderer.RenderStatus(w, http.StatusInternalServerError)
				return
			}

			// Session was created after pass change - we should expire it
			if identity.LastPasswordChange != nil && *identity.LastPasswordChange > claims.IssuedAt {
				err := redisQ.DeleteSession(claims.SessionID)
				if err != nil {
					log.WithError(err).Error("failed to delete session")
					renderer.RenderStatus(w, http.StatusInternalServerError)
					return
				}

				renderer.RenderStatus(w, http.StatusUnauthorized)
				return
			}

			if !identity.IsVerified {
				renderer.RenderErr(w, renderer.NewJsonErr(api.ErrUserNotVerified)...)
				return
			}

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}
