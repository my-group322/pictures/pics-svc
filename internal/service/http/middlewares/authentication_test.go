package middlewares

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/constants"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data/redis"

	"github.com/alicebob/miniredis/v2"
	redislib "github.com/go-redis/redis/v8"

	"github.com/dgrijalva/jwt-go"
	jwt2 "gitlab.com/my-group322/pictures/auth-svc/pkg/jwt"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/config"
)

func TestAuthenticationMiddleware(t *testing.T) {
	miniRedis, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer miniRedis.Close()

	redisClient := redislib.NewClient(&redislib.Options{
		Addr: miniRedis.Addr(),
	})

	var (
		a = &app.App{
			Log: logrus.New(),
			Cfg: &config.Config{
				InternalAccessKey: uuid.NewString(),
				JwtSecret:         uuid.NewString(),
			},
			RedisQ: redis.NewRedisQ(redisClient),
		}
		userID    = uuid.NewString()
		sessionID = int64(1)
	)

	err = redisClient.HSet(context.TODO(), identityRedisKey(userID), constants.RedisLastPassChangeKey, "", constants.RedisIsVerifiedKey, "1").Err()
	require.NoError(t, err)

	_, err = redisClient.SAdd(context.TODO(), "active:sessions", strconv.Itoa(int(sessionID))).Result()
	require.NoError(t, err)

	nextHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusUnavailableForLegalReasons)
	})

	t.Run("positive_by_internal_key", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodGet, "users/1", nil)
		require.NoError(t, err)

		req.Header.Set("Authorization", a.Cfg.InternalAccessKey)
		response := httptest.NewRecorder()

		AuthenticationMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusUnavailableForLegalReasons, response.Code)
	})

	t.Run("positive", func(t *testing.T) {
		claims := &jwt2.CustomClaimsJwt{
			UserID:         userID,
			SessionID:      sessionID,
			StandardClaims: jwt.StandardClaims{IssuedAt: time.Now().Unix()},
		}

		token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(a.Cfg.JwtSecret))
		require.NoError(t, err)

		req, err := http.NewRequest(http.MethodGet, "users/1", nil)
		require.NoError(t, err)

		req.Header.Set("Cookie", fmt.Sprintf("%s=%s", "jwt-auth", token))

		response := httptest.NewRecorder()

		AuthenticationMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusUnavailableForLegalReasons, response.Code)
	})

	t.Run("negative_invalid_token_secret_key", func(t *testing.T) {
		claims := &jwt2.CustomClaimsJwt{
			UserID:         userID,
			SessionID:      1,
			StandardClaims: jwt.StandardClaims{IssuedAt: time.Now().Unix()},
		}

		token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(uuid.NewString()))
		require.NoError(t, err)

		req, err := http.NewRequest(http.MethodGet, "users/1", nil)
		require.NoError(t, err)

		req.Header.Set("Cookie", fmt.Sprintf("%s=%s", "jwt-auth", token))

		response := httptest.NewRecorder()

		AuthenticationMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusUnauthorized, response.Code)
	})

	t.Run("negative_no_token", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodGet, "users/1", nil)
		require.NoError(t, err)

		response := httptest.NewRecorder()

		AuthenticationMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusUnauthorized, response.Code)
	})

	t.Run("negative_invalid_session", func(t *testing.T) {
		claims := &jwt2.CustomClaimsJwt{
			UserID:         userID,
			SessionID:      sessionID + 1,
			StandardClaims: jwt.StandardClaims{IssuedAt: time.Now().Unix()},
		}

		token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(a.Cfg.JwtSecret))
		require.NoError(t, err)

		req, err := http.NewRequest(http.MethodGet, "users/1", nil)
		require.NoError(t, err)

		req.Header.Set("Cookie", fmt.Sprintf("%s=%s", "jwt-auth", token))

		response := httptest.NewRecorder()

		AuthenticationMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusUnauthorized, response.Code)
	})

	t.Run("negative_user_not_verified", func(t *testing.T) {
		userID := uuid.NewString()

		err = redisClient.HSet(context.TODO(), identityRedisKey(userID), constants.RedisLastPassChangeKey, "", constants.RedisIsVerifiedKey, "0").Err()
		require.NoError(t, err)

		claims := &jwt2.CustomClaimsJwt{
			UserID:         userID,
			SessionID:      sessionID,
			StandardClaims: jwt.StandardClaims{IssuedAt: time.Now().Unix()},
		}

		token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(a.Cfg.JwtSecret))
		require.NoError(t, err)

		req, err := http.NewRequest(http.MethodGet, "users/1", nil)
		require.NoError(t, err)

		req.Header.Set("Cookie", fmt.Sprintf("%s=%s", "jwt-auth", token))

		response := httptest.NewRecorder()

		AuthenticationMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_pass_changed", func(t *testing.T) {
		userID := uuid.NewString()
		now := time.Now().Unix()

		err = redisClient.HSet(context.TODO(), identityRedisKey(userID), constants.RedisLastPassChangeKey, now+1, constants.RedisIsVerifiedKey, "1").Err()
		require.NoError(t, err)

		claims := &jwt2.CustomClaimsJwt{
			UserID:         userID,
			SessionID:      sessionID,
			StandardClaims: jwt.StandardClaims{IssuedAt: now},
		}

		token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(a.Cfg.JwtSecret))
		require.NoError(t, err)

		req, err := http.NewRequest(http.MethodGet, "users/1", nil)
		require.NoError(t, err)

		req.Header.Set("Cookie", fmt.Sprintf("%s=%s", "jwt-auth", token))

		response := httptest.NewRecorder()

		AuthenticationMiddleware(a)(nextHandler).ServeHTTP(response, req)
		require.Equal(t, http.StatusUnauthorized, response.Code)
	})
}

func identityRedisKey(accountID string) string {
	return fmt.Sprintf("%s:%s", constants.RedisUsersKey, accountID)
}
