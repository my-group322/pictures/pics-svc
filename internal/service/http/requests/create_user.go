package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation"
)

type CreateUser struct {
	Data resources.User `json:"data"`
}

func NewCreateUser(r *http.Request) (*CreateUser, error) {
	var request CreateUser

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return &request, validation.Errors{"request": err}
	}

	return &request, request.validate()
}

func (r *CreateUser) validate() error {
	errs := validation.Errors{
		"/data/id":                    validation.Validate(r.Data.ID, validation.Required),
		"/data/attributes/email":      validation.Validate(r.Data.Attributes.Email, validation.Required),
		"/data/attributes/first_name": validation.Validate(r.Data.Attributes.FirstName, validation.Required),
		"/data/attributes/last_name":  validation.Validate(r.Data.Attributes.LastName, validation.Required),
	}

	return errs.Filter()
}
