package requests

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/go-chi/chi/v5"
	validation "github.com/go-ozzo/ozzo-validation"
)

type UpdatePicture struct {
	PictureID int64
	Data      resources.Picture `json:"data"`
}

func NewUpdatePicture(r *http.Request) (UpdatePicture, error) {
	var request UpdatePicture

	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		return request, validation.Errors{"id": err}
	}
	request.PictureID = id

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{"request": err}
	}

	return request, request.validate()
}

func (r *UpdatePicture) validate() error {
	errs := validation.Errors{
		"/data/attributes/comment": validation.Validate(r.Data.Attributes.Comment, validation.Length(0, 300)),
	}

	return errs.Filter()
}
