package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/go-chi/chi/v5"
	validation "github.com/go-ozzo/ozzo-validation"
)

type UpdateProfile struct {
	UserID string
	Data   resources.User `json:"data"`
}

func NewUpdateProfile(r *http.Request) (UpdateProfile, error) {
	var request UpdateProfile

	request.UserID = chi.URLParam(r, "id")

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{"request": err}
	}

	return request, request.validate()
}

func (r *UpdateProfile) validate() error {
	errs := validation.Errors{
		"/data/attributes/first_name": validation.Validate(r.Data.Attributes.FirstName, validation.Required),
		"/data/attributes/last_name":  validation.Validate(r.Data.Attributes.LastName, validation.Required),
	}

	return errs.Filter()
}
