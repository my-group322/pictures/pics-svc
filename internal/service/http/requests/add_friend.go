package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/asaskevich/govalidator"

	validation "github.com/go-ozzo/ozzo-validation"
)

var uuidValidation = validation.NewStringRule(govalidator.IsUUIDv4, "id should be valid uuid")

type AddFriend struct {
	Data resources.User `json:"data"`
}

func NewAddFriend(r *http.Request) (AddFriend, error) {
	var request AddFriend

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{"request": err}
	}

	return request, request.validate()
}

func (r *AddFriend) validate() error {
	errs := validation.Errors{
		"/data/id": validation.Validate(r.Data.ID,
			validation.Required,
			uuidValidation,
		),
	}

	return errs.Filter()
}
