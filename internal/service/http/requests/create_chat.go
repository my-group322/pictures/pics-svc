package requests

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation"
)

type CreateChat struct {
	Data resources.Picture `json:"data"`
}

func NewCreateChat(r *http.Request) (CreateChat, error) {
	var request CreateChat

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request, validation.Errors{"request": err}
	}

	return request, request.validate()
}

func (r *CreateChat) validate() error {
	errs := validation.Errors{
		"/data/id": validation.Validate(r.Data.ID, validation.Required),
	}

	return errs.Filter()
}
