package chat

import (
	"context"
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	redislib "github.com/go-redis/redis/v8"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"golang.org/x/sync/errgroup"

	"github.com/pkg/errors"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"github.com/sirupsen/logrus"
)

type Handler struct {
	log   *logrus.Logger
	api   api.API
	redis *redislib.Client
}

func NewHandler(redis *redislib.Client, log *logrus.Logger, api api.API) *Handler {
	return &Handler{redis: redis, log: log, api: api}
}

func (h Handler) Handle(w http.ResponseWriter, r *http.Request) {
	conn, _, _, err := ws.UpgradeHTTP(r, w)
	if err != nil {
		h.log.WithError(err).Error("failed to upgrade")
		return
	}
	defer conn.Close()

	requester := ctx2.Requester(r.Context())
	log := h.log.WithField("requester", requester)

	client := userClient{
		userID:    requester,
		log:       log,
		writeChan: make(chan []byte),
		redis:     h.redis,
		pubSub:    h.redis.Subscribe(context.TODO(), requester),
	}

	errGroup, ctx := errgroup.WithContext(context.Background())

	go client.ListenToMessagesFromPeers()
	go client.SendAvailableMessagesFromPeersToUser(ctx, errGroup, conn)
	defer client.Shutdown()

	errGroup.Go(func() error {
		for {
			if ctx.Err() != nil {
				return nil
			}

			messageRaw, _, err := wsutil.ReadClientData(conn)
			if err != nil {
				// Ignoring empty messages
				if !errors.Is(err, io.EOF) {
					log.WithError(err).Error("failed to read message")
					return err
				}
				continue
			}

			var message resources.SendMessage
			if err = json.Unmarshal(messageRaw, &message); err != nil {
				log.WithError(err).Error("failed to unmarshal message")
				continue
			}

			receiverID, err := h.api.CreateMessage(r.Context(), &message)
			if err != nil {
				continue
			}

			client.SendMessageToPeer(receiverID, messageRaw)
		}
	})

	if err = errGroup.Wait(); err != nil {
		log.WithError(err).Error("some goroutine failed, exiting ws handler")
	}
}
