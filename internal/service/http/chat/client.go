package chat

import (
	"context"
	"net"

	"golang.org/x/sync/errgroup"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"

	redislib "github.com/go-redis/redis/v8"
	"github.com/sirupsen/logrus"
)

type userClient struct {
	userID    string
	log       *logrus.Entry
	writeChan chan []byte // Direct concurrent writing to net.Conn is not safe
	redis     *redislib.Client
	pubSub    *redislib.PubSub
}

func (u userClient) ListenToMessagesFromPeers() {
	messages := u.pubSub.Channel()

	for {
		message, ok := <-messages
		if !ok {
			return
		}

		u.writeChan <- []byte(message.Payload)
	}
}

func (u userClient) Shutdown() {
	if err := u.pubSub.Unsubscribe(context.Background(), u.userID); err != nil {
		u.log.WithError(err).Error("failed to unsubscribe from topics")
	}

	if err := u.pubSub.Close(); err != nil {
		u.log.WithError(err).Error("failed to close pubsub")
	}
}

func (u userClient) SendAvailableMessagesFromPeersToUser(ctx context.Context, errGroup *errgroup.Group, conn net.Conn) {
	errGroup.Go(func() error {
		for {
			select {
			case <-ctx.Done():
				return nil
			case message := <-u.writeChan:
				err := wsutil.WriteServerMessage(conn, ws.OpText, message)
				if err != nil {
					u.log.WithError(err).Error("failed to write message")
					return err
				}
			}
		}
	})
}

func (u userClient) SendMessageToPeer(userID string, message []byte) {
	if err := u.redis.Publish(context.TODO(), userID, message).Err(); err != nil {
		u.log.WithError(err).Error("failed to publish message to topic")
	}
}
