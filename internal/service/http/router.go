package http

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	nrcontext "github.com/ilyasiv2003/newrelic-context"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/chat"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/middlewares"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/server"
)

func Router(a *app.App, server *server.Server, chatClient *chat.Handler) chi.Router {
	r := chi.NewRouter()

	r.Use(
		middleware.Heartbeat("/ping"),
		nrcontext.NewMiddlewareWithApp(a.Metrics).Handler,
		middlewares.AuthenticationMiddleware(a),
		middleware.Recoverer,
	)

	r.Route("/pictures", func(r chi.Router) {
		// Returns thumbnails of pictures, for full size use GET pictures/{id}
		r.Get("/", server.GetPicsList)
		r.Post("/", server.CreatePicture)

		r.Route("/{id}", func(r chi.Router) {
			r.Get("/", server.GetPicture)
			r.Patch("/", server.UpdatePicture)
			r.Delete("/", server.DeletePicture)

			r.Route("/like", func(r chi.Router) {
				r.Post("/", server.CreateLike)
				r.Delete("/", server.DeleteLike)
			})
		})
	})

	r.Route("/users", func(r chi.Router) {
		r.Get("/", server.GetUserList)

		r.Route("/{id}", func(r chi.Router) {
			r.Get("/", server.GetUser)

			r.Put("/avatar", server.UpdateAvatar)
			r.Patch("/profile", server.UpdateProfile)

			r.Route("/friends", func(r chi.Router) {
				r.Post("/", server.AddFriend)
				r.Delete("/", server.DeleteFriend)
			})
		})
	})

	r.Route("/chats", func(r chi.Router) {
		r.Get("/", server.GetChatList)
		r.Get("/{id}", server.GetChatHistory)

		r.Post("/", server.CreateChat)
	})

	r.HandleFunc("/chat", chatClient.Handle)

	// Internal endpoints, not for client, only backend
	r.Group(func(r chi.Router) {
		r.Use(middlewares.InternalAccessGuardMiddleware(a))

		r.Post("/users", server.CreateUser)
	})

	return r
}
