package server

import (
	"encoding/json"
	"net/http"
	"testing"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/stretchr/testify/mock"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"

	"github.com/stretchr/testify/require"

	"github.com/google/uuid"
)

func TestServer_DeleteFriend(t *testing.T) {
	var (
		log      = logrus.New()
		mockApi  = api.MockAPI{}
		server   = New(&mockApi, log)
		friendID = uuid.NewString()
		userID   = uuid.NewString()
	)

	bodyValidReq, err := json.Marshal(resources.UserResponse{
		Data: resources.User{
			Key: jares.Key{
				ID:   friendID,
				Type: resources.ResourceTypeUser,
			},
		},
	})
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		mockApi.On("DeleteFriend", mock.Anything, friendID).Return(nil).Once()

		response := recordHandler(server.DeleteFriend, bodyValidReq, userID)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_not_uuid", func(t *testing.T) {
		body, err := json.Marshal(resources.UserResponse{
			Data: resources.User{
				Key: jares.Key{
					ID:   "1",
					Type: resources.ResourceTypeUser,
				},
			},
		})
		require.NoError(t, err)
		response := recordHandler(server.DeleteFriend, body, userID)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})

	t.Run("negative_handler_forbidden_error", func(t *testing.T) {
		mockApi.On("DeleteFriend", mock.Anything, friendID).Return(api.ErrAlreadyFriend).Once()

		response := recordHandler(server.DeleteFriend, bodyValidReq, userID)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_handler_internal_error", func(t *testing.T) {
		mockApi.On("DeleteFriend", mock.Anything, friendID).Return(errors.New("")).Once()

		response := recordHandler(server.DeleteFriend, bodyValidReq, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})
}
