package server

import (
	"bytes"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app/mocks/s3mock"

	"gitlab.com/my-group322/pictures/pics-svc/internal/config"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/pkg/errors"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/mock"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

const (
	validPicturePath = "../../../../assets/sample_picture.jpg"
	notPicturePath   = "../../../../assets/not_picture.txt"
)

func TestServer_CreatePicture(t *testing.T) {
	var (
		log            = logrus.New()
		userID         = uuid.NewString()
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		uploader       = &s3mock.UploaderAPI{}
		pictureID      = int64(1)
	)

	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)

	newAPI := api.New(&app.App{
		Cfg:        &config.Config{},
		Log:        log,
		PicturesQ:  picturesQClone,
		S3Uploader: uploader,
	})
	server := New(newAPI, log)

	reqValidPic := prepareMultipartFileRequest(t, validPicturePath, "picture")

	t.Run("positive", func(t *testing.T) {
		uploader.On("Upload", mock.Anything).Return(nil, nil).Once()

		picturesQ.On("Create", mock.Anything).Return(pictureID, nil).Once()

		picturesQ.On("Transaction", mock.Anything).Run(func(args mock.Arguments) {
			txCallback, ok := args.Get(0).(func(tx data.PicturesQ) error)
			require.True(t, ok)

			err := txCallback(picturesQ)
			require.NoError(t, err)
		}).Return(nil).Once()

		response := recordHandlerWithRequest(server.CreatePicture, reqValidPic, userID)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_no_file_attached", func(t *testing.T) {
		response := recordHandler(server.CreatePicture, nil, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})

	t.Run("negative_not_picture_attached", func(t *testing.T) {
		req := prepareMultipartFileRequest(t, notPicturePath, "picture")
		response := recordHandlerWithRequest(server.CreatePicture, req, userID)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_tx_failed", func(t *testing.T) {
		picturesQ.On("Transaction", mock.Anything).Return(errors.New(""))

		response := recordHandlerWithRequest(server.CreatePicture, reqValidPic, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})
}

func prepareMultipartFileRequest(t *testing.T, filePath string, fileFieldName string) *http.Request {
	picture, err := os.Open(filePath)
	require.NoError(t, err)

	defer picture.Close()

	formBuf := bytes.NewBuffer(nil)
	w := multipart.NewWriter(formBuf)
	formWriter, err := w.CreateFormFile(fileFieldName, picture.Name())
	require.NoError(t, err)

	_, err = io.Copy(formWriter, picture)
	require.NoError(t, err)

	err = w.Close()
	require.NoError(t, err)

	req, err := http.NewRequest("", "", formBuf)
	require.NoError(t, err)

	// Required header for form data requests
	req.Header.Add("Content-Type", w.FormDataContentType())

	return req
}
