package server

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	// Decoder requires them be imported like this to be recognized
	_ "image/jpeg"
	_ "image/png"

	"github.com/go-chi/chi/v5"
)

func (s Server) UpdateAvatar(w http.ResponseWriter, r *http.Request) {
	userID := chi.URLParam(r, "id")

	file, header, err := r.FormFile("avatar")
	if err != nil {
		s.log.WithError(err).Error("failed to form file")
		renderer.RenderStatus(w, http.StatusInternalServerError)
		return
	}

	response, err := s.api.UpdateAvatar(r.Context(), file, header, userID)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	_ = json.NewEncoder(w).Encode(response)
}
