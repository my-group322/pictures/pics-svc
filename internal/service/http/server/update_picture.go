package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

func (s Server) UpdatePicture(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewUpdatePicture(r)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.BadRequestErr(err)...)
		return
	}

	if err = s.api.UpdatePicture(r.Context(), request.PictureID, &request.Data); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}
}
