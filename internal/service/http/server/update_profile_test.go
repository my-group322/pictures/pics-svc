package server

import (
	"encoding/json"
	"net/http"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/stretchr/testify/mock"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestServer_UpdateProfile(t *testing.T) {
	var (
		log         = logrus.New()
		usersQClone = &datamock.UsersQCloner{}
		usersQ      = &datamock.UsersQ{}
		userID      = uuid.NewString()
	)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)

	newAPI := api.New(&app.App{
		Log:    log,
		UsersQ: usersQClone,
	})
	server := New(newAPI, log)

	chiContext := &chi.Context{}
	chiContext.URLParams.Add("id", userID)

	bodyValidReq, err := json.Marshal(resources.UserResponse{
		Data: resources.User{
			Attributes: resources.UserAttributes{
				Email:     uuid.NewString(),
				FirstName: "Illia",
				LastName:  "Syvoshapka",
			},
		},
	})
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(&data.User{}, nil).Once()
		usersQ.On("Update", mock.Anything).Return(nil).Once()

		response := recordHandler(server.UpdateProfile, bodyValidReq, userID, chiContext)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_not_owned_profile", func(t *testing.T) {
		response := recordHandler(server.UpdateProfile, bodyValidReq, uuid.NewString(), chiContext)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_invalid_body", func(t *testing.T) {
		response := recordHandler(server.UpdateProfile, []byte("nope"), userID, chiContext)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})

	t.Run("negative_profile_not_found", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(nil, nil).Once()

		response := recordHandler(server.UpdateProfile, bodyValidReq, userID, chiContext)
		require.Equal(t, http.StatusNotFound, response.Code)
	})
}
