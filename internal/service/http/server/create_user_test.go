package server

import (
	"encoding/json"
	"net/http"
	"testing"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestServer_CreateUser(t *testing.T) {
	var (
		log         = logrus.New()
		userID      = uuid.NewString()
		userEmail   = uuid.NewString()
		usersQClone = &datamock.UsersQCloner{}
		usersQ      = &datamock.UsersQ{}
	)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)

	newAPI := api.New(&app.App{
		Log:    log,
		UsersQ: usersQClone,
	})
	server := New(newAPI, log)

	bodyValidReq, err := json.Marshal(resources.UserResponse{
		Data: resources.User{
			Key: jares.Key{
				ID:   userID,
				Type: resources.ResourceTypeUser,
			},
			Attributes: resources.UserAttributes{
				Email:     userEmail,
				FirstName: "Illia",
				LastName:  "Syvoshapka",
			},
		},
	})
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		usersQ.On("Create", mock.Anything).Return(nil).Once()

		response := recordHandler(server.CreateUser, bodyValidReq, userID)
		require.Equal(t, http.StatusCreated, response.Code)
	})

	t.Run("negative_no_id", func(t *testing.T) {
		req, err := json.Marshal(resources.UserResponse{
			Data: resources.User{
				Key: jares.Key{
					ID:   "",
					Type: resources.ResourceTypeUser,
				},
				Attributes: resources.UserAttributes{
					Email:     userEmail,
					FirstName: "Illia",
					LastName:  "Syvoshapka",
				},
			},
		})
		require.NoError(t, err)

		response := recordHandler(server.CreateUser, req, userID)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})

	t.Run("negative_conflict", func(t *testing.T) {
		usersQ.On("Create", mock.Anything).Return(pkgdata.ErrUniqueViolationError).Once()

		response := recordHandler(server.CreateUser, bodyValidReq, userID)
		require.Equal(t, http.StatusConflict, response.Code)
	})
}
