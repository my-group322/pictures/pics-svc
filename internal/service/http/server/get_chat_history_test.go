package server

import (
	"net/http"
	"strconv"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestServer_GetChatHistory(t *testing.T) {
	var (
		log     = logrus.New()
		mockAPI = api.MockAPI{}
		server  = New(&mockAPI, log)
		chatID  = int64(1)
		userID  = uuid.NewString()
	)

	chiContext := &chi.Context{}
	chiContext.URLParams.Add("id", strconv.FormatInt(chatID, 10))

	t.Run("positive", func(t *testing.T) {
		mockAPI.On("GetChatHistory", mock.Anything, mock.Anything, chatID).Return(nil, nil).Once()

		response := recordHandler(server.GetChatHistory, nil, userID, chiContext)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_handler_error", func(t *testing.T) {
		mockAPI.On("GetChatHistory", mock.Anything, mock.Anything, chatID).Return(nil, errors.New("")).Once()

		response := recordHandler(server.GetChatHistory, nil, userID, chiContext)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})
}
