package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

func (s Server) AddFriend(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewAddFriend(r)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(err)...)
		return
	}

	if err = s.api.AddFriend(r.Context(), request.Data.ID); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}
}
