package server

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/go-chi/chi/v5"
)

func (s Server) GetUser(w http.ResponseWriter, r *http.Request) {
	response, err := s.api.GetUser(r.Context(), chi.URLParam(r, "id"))
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	_ = json.NewEncoder(w).Encode(response)
}
