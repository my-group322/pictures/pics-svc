package server

import (
	"context"
	"net/http"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app/mocks/s3mock"

	"gitlab.com/my-group322/pictures/pics-svc/internal/config"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/aws/aws-sdk-go/service/s3/s3manager"

	"github.com/go-chi/chi/v5"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestServer_UpdateAvatar(t *testing.T) {
	var (
		log         = logrus.New()
		userID      = uuid.NewString()
		usersQClone = &datamock.UsersQCloner{}
		usersQ      = &datamock.UsersQ{}
		uploader    = &s3mock.UploaderAPI{}
	)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)

	newAPI := api.New(&app.App{
		Cfg:        &config.Config{},
		Log:        log,
		UsersQ:     usersQClone,
		S3Uploader: uploader,
	})
	server := New(newAPI, log)

	chiContext := &chi.Context{}
	chiContext.URLParams.Add("id", userID)

	reqValidPic := prepareMultipartFileRequest(t, validPicturePath, "avatar")
	reqValidPic = reqValidPic.WithContext(context.WithValue(reqValidPic.Context(), chi.RouteCtxKey, chiContext))

	t.Run("positive_didnt_have_avatar", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(&data.User{
			ID:        userID,
			HasAvatar: false,
		}, nil).Once()
		uploader.On("Upload", mock.Anything).Return(&s3manager.UploadOutput{Location: "http//..."}, nil).Once()
		usersQ.On("UpdateHasAvatar", userID, true).Return(nil).Once()

		response := recordHandlerWithRequest(server.UpdateAvatar, reqValidPic, userID)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("positive_had_avatar", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(&data.User{
			ID:        userID,
			HasAvatar: true,
		}, nil).Once()
		uploader.On("Upload", mock.Anything).Return(&s3manager.UploadOutput{Location: "http//..."}, nil).Once()

		reqValidPic = prepareMultipartFileRequest(t, validPicturePath, "avatar")
		reqValidPic = reqValidPic.WithContext(context.WithValue(reqValidPic.Context(), chi.RouteCtxKey, chiContext))

		response := recordHandlerWithRequest(server.UpdateAvatar, reqValidPic, userID)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_no_file_attached", func(t *testing.T) {
		response := recordHandler(server.UpdateAvatar, nil, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})

	t.Run("negative_not_own_avatar", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(&data.User{
			ID:        uuid.NewString(),
			HasAvatar: false,
		}, nil).Once()

		reqValidPic = prepareMultipartFileRequest(t, validPicturePath, "avatar")
		reqValidPic = reqValidPic.WithContext(context.WithValue(reqValidPic.Context(), chi.RouteCtxKey, chiContext))

		response := recordHandlerWithRequest(server.UpdateAvatar, reqValidPic, userID)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_user_not_found", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(nil, nil).Once()

		reqValidPic = prepareMultipartFileRequest(t, validPicturePath, "avatar")
		reqValidPic = reqValidPic.WithContext(context.WithValue(reqValidPic.Context(), chi.RouteCtxKey, chiContext))

		response := recordHandlerWithRequest(server.UpdateAvatar, reqValidPic, userID)
		require.Equal(t, http.StatusNotFound, response.Code)
	})
}
