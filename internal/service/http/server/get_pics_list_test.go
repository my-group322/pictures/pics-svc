package server

import (
	"archive/zip"
	"bytes"
	"io/ioutil"
	"net/http"
	"strconv"
	"testing"
	"time"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"github.com/pkg/errors"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func TestServer_GetPicsList(t *testing.T) {
	var (
		log       = logrus.New()
		mockAPI   = api.MockAPI{}
		server    = New(&mockAPI, log)
		userID    = uuid.NewString()
		pictureID = int64(1)
		thumbnail = "thumbnail"
	)

	t.Run("positive", func(t *testing.T) {
		mockAPI.On("GetPicsList", mock.Anything, mock.Anything).Return([]data.Picture{{
			ID:        pictureID,
			CreatedAt: time.Now(),
			Thumbnail: []byte(thumbnail),
		}}, int64(1), nil).Once()

		response := recordHandler(server.GetPicsList, nil, userID)
		require.Equal(t, http.StatusOK, response.Code)
		require.Equal(t, "1", response.Header().Get("Total"))

		body, err := ioutil.ReadAll(response.Body)
		require.NoError(t, err)

		zipReader, err := zip.NewReader(bytes.NewReader(body), int64(len(body)))
		require.NoError(t, err)

		for _, zipFile := range zipReader.File {
			require.Equal(t, strconv.FormatInt(pictureID, 10)+".jpg", zipFile.Name)

			fileBytes, err := readZipFile(zipFile)
			require.NoError(t, err)

			require.Equal(t, []byte(thumbnail), fileBytes)
		}
	})

	t.Run("negative_handler_err", func(t *testing.T) {
		mockAPI.On("GetPicsList", mock.Anything, mock.Anything).Return(nil, int64(0), errors.New("")).Once()

		response := recordHandler(server.GetPicsList, nil, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})
}

func readZipFile(zf *zip.File) ([]byte, error) {
	f, err := zf.Open()
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return ioutil.ReadAll(f)
}
