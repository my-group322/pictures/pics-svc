package server

import (
	"net/http"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"github.com/stretchr/testify/mock"

	"github.com/go-chi/chi/v5"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
)

func TestServer_GetUser(t *testing.T) {
	var (
		log     = logrus.New()
		mockAPI = api.MockAPI{}
		server  = New(&mockAPI, log)
		userID  = uuid.NewString()
	)

	chiContext := &chi.Context{}
	chiContext.URLParams.Add("id", userID)

	t.Run("positive", func(t *testing.T) {
		mockAPI.On("GetUser", mock.Anything, userID).Return(nil, nil).Once()

		response := recordHandler(server.GetUser, nil, userID, chiContext)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_handler_error", func(t *testing.T) {
		mockAPI.On("GetUser", mock.Anything, userID).Return(nil, api.ErrUserNotFound).Once()

		response := recordHandler(server.GetUser, nil, userID, chiContext)
		require.Equal(t, http.StatusNotFound, response.Code)
	})
}
