package server

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/gorilla/schema"
)

func (s Server) GetUserList(w http.ResponseWriter, r *http.Request) {
	var params resources.QueryParams
	_ = schema.NewDecoder().Decode(&params, r.URL.Query())

	response, err := s.api.GetUserList(r.Context(), params)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	_ = json.NewEncoder(w).Encode(response)
}
