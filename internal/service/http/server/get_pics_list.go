package server

import (
	"archive/zip"
	"bytes"
	"io"
	"net/http"
	"strconv"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/pkg/fakes"

	"github.com/gorilla/schema"
)

func (s Server) GetPicsList(w http.ResponseWriter, r *http.Request) {
	var params resources.QueryParams
	_ = schema.NewDecoder().Decode(&params, r.URL.Query())

	pictures, picsCount, err := s.api.GetPicsList(r.Context(), params)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	w.Header().Set("Total", strconv.FormatInt(picsCount, 10))

	// Zipping pictures
	zipWriter := zip.NewWriter(w)
	defer zipWriter.Close()

	for i := range pictures {
		fileInfo := fakes.NewFakeFileInfo(strconv.FormatInt(pictures[i].ID, 10)+".jpg", pictures[i].CreatedAt)
		header, _ := zip.FileInfoHeader(fileInfo)

		writer, err := zipWriter.CreateHeader(header)
		if err != nil {
			s.log.WithError(err).Error("failed to create header")
			renderer.RenderStatus(w, http.StatusInternalServerError)
			return
		}

		_, err = io.Copy(writer, bytes.NewBuffer(pictures[i].Thumbnail))
		if err != nil {
			s.log.WithError(err).Error("failed to add file to zip")
			renderer.RenderStatus(w, http.StatusInternalServerError)
			return
		}
	}

	w.Header().Set("Content-Type", "application/zip")
	w.Header().Set("Content-Disposition", "attachment; filename=pics.zip")
}
