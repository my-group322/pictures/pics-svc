package server

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	validation "github.com/go-ozzo/ozzo-validation"
)

func (s Server) CreateChat(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateChat(r)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(err)...)
		return
	}

	picID, err := strconv.ParseInt(request.Data.ID, 10, 64)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(validation.Errors{"data/id": err})...)
		return
	}

	response, err := s.api.CreateChat(r.Context(), picID)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	_ = json.NewEncoder(w).Encode(response)
}
