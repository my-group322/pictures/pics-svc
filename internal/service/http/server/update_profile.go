package server

import (
	"net/http"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/http/requests"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

func (s Server) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewUpdateProfile(r)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.BadRequestErr(err)...)
		return
	}

	if err = s.api.UpdateProfile(r.Context(), request.UserID, &request.Data); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}
}
