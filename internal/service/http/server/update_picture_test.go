package server

import (
	"encoding/json"
	"net/http"
	"strconv"
	"testing"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/go-chi/chi/v5"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestServer_UpdatePicture(t *testing.T) {
	var (
		log            = logrus.New()
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		ownerID        = uuid.NewString()
		userID         = uuid.NewString()
		pictureID      = int64(1)
	)

	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)

	newAPI := api.New(&app.App{
		Log:       log,
		PicturesQ: picturesQClone,
	})
	server := New(newAPI, log)

	chiContext := &chi.Context{}
	chiContext.URLParams.Add("id", strconv.FormatInt(pictureID, 10))

	bodyValidReq, err := json.Marshal(resources.PictureResponse{
		Data: resources.Picture{
			Key: jares.Key{
				ID:   strconv.FormatInt(pictureID, 10),
				Type: resources.ResourceTypePicture,
			},
			Attributes: resources.PictureAttributes{
				Privacy: data.PicturePrivacyPublic.Int64(),
			},
		},
	})
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		picturesQ.On("GetByID", pictureID).Return(&data.Picture{
			ID:      pictureID,
			Owner:   userID,
			Privacy: data.PicturePrivacyPublic,
		}, nil).Once()

		picturesQ.On("GetPermittedUsers", pictureID).Return(nil, nil).Once()
		picturesQ.On("Update", mock.Anything).Return(nil).Once()

		response := recordHandler(server.UpdatePicture, bodyValidReq, userID, chiContext)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("positive_updated_permitted_users", func(t *testing.T) {
		newPermittedUserID := uuid.NewString()

		req, err := json.Marshal(resources.PictureResponse{
			Data: resources.Picture{
				Key: jares.Key{
					ID:   strconv.FormatInt(pictureID, 10),
					Type: resources.ResourceTypePicture,
				},
				Attributes: resources.PictureAttributes{
					Privacy: data.PicturePrivacyToSelectedUsers.Int64(),
				},
				Relationships: resources.PictureRelationships{
					SharedWith: jares.RelationCollection{
						Data: []jares.Key{{
							ID:   newPermittedUserID,
							Type: resources.ResourceTypeUser,
						}},
					},
				},
			},
		})
		require.NoError(t, err)

		picturesQ.On("GetByID", pictureID).Return(&data.Picture{
			ID:      pictureID,
			Owner:   userID,
			Privacy: data.PicturePrivacyToSelectedUsers,
		}, nil).Once()

		picturesQ.On("GetPermittedUsers", pictureID).Return([]string{uuid.NewString()}, nil).Once()
		picturesQ.On("UpdateViewPermissions", pictureID, []string{newPermittedUserID}).Return(nil).Once()
		picturesQ.On("Update", mock.Anything).Return(nil).Once()

		response := recordHandler(server.UpdatePicture, req, userID, chiContext)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_not_owner", func(t *testing.T) {
		picturesQ.On("GetByID", pictureID).Return(&data.Picture{
			ID:      pictureID,
			Owner:   ownerID,
			Privacy: data.PicturePrivacyPublic,
		}, nil).Once()

		response := recordHandler(server.UpdatePicture, bodyValidReq, userID, chiContext)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_picture_not_found", func(t *testing.T) {
		picturesQ.On("GetByID", pictureID).Return(nil, nil).Once()

		response := recordHandler(server.UpdatePicture, bodyValidReq, userID, chiContext)
		require.Equal(t, http.StatusNotFound, response.Code)
	})

	t.Run("negative_not_empty_shared_with_privacy_not_to_selected", func(t *testing.T) {
		req, err := json.Marshal(resources.PictureResponse{
			Data: resources.Picture{
				Key: jares.Key{
					ID:   strconv.FormatInt(pictureID, 10),
					Type: resources.ResourceTypePicture,
				},
				Attributes: resources.PictureAttributes{
					Privacy: data.PicturePrivacyPublic.Int64(),
				},
				Relationships: resources.PictureRelationships{
					SharedWith: jares.RelationCollection{
						Data: []jares.Key{{
							ID:   uuid.NewString(),
							Type: resources.ResourceTypeUser,
						}},
					},
				},
			},
		})
		require.NoError(t, err)

		picturesQ.On("GetByID", pictureID).Return(&data.Picture{
			ID:      pictureID,
			Owner:   userID,
			Privacy: data.PicturePrivacyPublic,
		}, nil).Once()

		response := recordHandler(server.UpdatePicture, req, userID, chiContext)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})

	t.Run("negative_invalid_body", func(t *testing.T) {
		response := recordHandler(server.UpdatePicture, []byte("nope"), userID, chiContext)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})
}
