package server

import (
	"encoding/json"
	"net/http"
	"strconv"
	"testing"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/pkg/errors"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

func TestServer_CreateChat(t *testing.T) {
	var (
		log       = logrus.New()
		mockAPI   = api.MockAPI{}
		server    = New(&mockAPI, log)
		pictureID = int64(1)
		userID    = uuid.NewString()
	)

	bodyValidReq, err := json.Marshal(resources.PictureResponse{
		Data: resources.Picture{
			Key: jares.Key{
				ID:   strconv.FormatInt(pictureID, 10),
				Type: resources.ResourceTypePicture,
			},
		},
	})
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		mockAPI.On("CreateChat", mock.Anything, pictureID).Return(nil, nil).Once()

		response := recordHandler(server.CreateChat, bodyValidReq, userID)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_invalid_picture_id", func(t *testing.T) {
		body, err := json.Marshal(resources.PictureResponse{
			Data: resources.Picture{
				Key: jares.Key{
					ID:   "something definitely not int",
					Type: resources.ResourceTypePicture,
				},
			},
		})
		require.NoError(t, err)

		response := recordHandler(server.CreateChat, body, userID)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})

	t.Run("negative_invalid_body", func(t *testing.T) {
		response := recordHandler(server.CreateChat, []byte("nope"), userID)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})

	t.Run("negative_handler_error", func(t *testing.T) {
		mockAPI.On("CreateChat", mock.Anything, pictureID).Return(nil, errors.New(""))

		response := recordHandler(server.CreateChat, bodyValidReq, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})
}
