package server

import (
	"net/http"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
)

func TestServer_GetUserList(t *testing.T) {
	var (
		log     = logrus.New()
		mockAPI = api.MockAPI{}
		server  = New(&mockAPI, log)
		userID  = uuid.NewString()
	)

	t.Run("positive", func(t *testing.T) {
		mockAPI.On("GetUserList", mock.Anything, mock.Anything).Return(nil, nil).Once()

		response := recordHandler(server.GetUserList, nil, userID)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_handler_error", func(t *testing.T) {
		mockAPI.On("GetUserList", mock.Anything, mock.Anything).Return(nil, errors.New("")).Once()

		response := recordHandler(server.GetUserList, nil, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})
}
