package server

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/go-chi/chi/v5"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/mock"

	"github.com/sirupsen/logrus"

	"github.com/stretchr/testify/require"

	"github.com/google/uuid"
)

func TestServer_AddFriend(t *testing.T) {
	var (
		log      = logrus.New()
		mockApi  = api.MockAPI{}
		server   = New(&mockApi, log)
		friendID = uuid.NewString()
		userID   = uuid.NewString()
	)

	bodyValidReq, err := json.Marshal(resources.UserResponse{
		Data: resources.User{
			Key: jares.Key{
				ID:   friendID,
				Type: resources.ResourceTypeUser,
			},
		},
	})
	require.NoError(t, err)

	t.Run("positive", func(t *testing.T) {
		mockApi.On("AddFriend", mock.Anything, friendID).Return(nil).Once()

		response := recordHandler(server.AddFriend, bodyValidReq, userID)
		require.Equal(t, http.StatusOK, response.Code)
	})

	t.Run("negative_not_uuid", func(t *testing.T) {
		body, err := json.Marshal(resources.UserResponse{
			Data: resources.User{
				Key: jares.Key{
					ID:   "1",
					Type: resources.ResourceTypeUser,
				},
			},
		})
		require.NoError(t, err)
		response := recordHandler(server.AddFriend, body, userID)
		require.Equal(t, http.StatusBadRequest, response.Code)
	})

	t.Run("negative_handler_forbidden_error", func(t *testing.T) {
		mockApi.On("AddFriend", mock.Anything, friendID).Return(api.ErrAlreadyFriend).Once()

		response := recordHandler(server.AddFriend, bodyValidReq, userID)
		require.Equal(t, http.StatusForbidden, response.Code)
	})

	t.Run("negative_handler_internal_error", func(t *testing.T) {
		mockApi.On("AddFriend", mock.Anything, friendID).Return(errors.New("")).Once()

		response := recordHandler(server.AddFriend, bodyValidReq, userID)
		require.Equal(t, http.StatusInternalServerError, response.Code)
	})
}

func recordHandler(handlerFunc func(w http.ResponseWriter, r *http.Request), body []byte, requester string, paramsCtx ...*chi.Context) *httptest.ResponseRecorder {
	request, _ := http.NewRequest("", "", bytes.NewBuffer(body))
	response := httptest.NewRecorder()

	ctx := ctx2.RequesterToCtx(requester)(request.Context())
	if paramsCtx != nil {
		ctx = context.WithValue(ctx, chi.RouteCtxKey, paramsCtx[0])
	}

	handler := http.HandlerFunc(handlerFunc)
	handler.ServeHTTP(response, request.WithContext(ctx))

	return response
}

func recordHandlerWithRequest(handlerFunc func(w http.ResponseWriter, r *http.Request), req *http.Request, requester string) *httptest.ResponseRecorder {
	response := httptest.NewRecorder()

	handler := http.HandlerFunc(handlerFunc)
	handler.ServeHTTP(response, req.WithContext(ctx2.RequesterToCtx(requester)(req.Context())))

	return response
}
