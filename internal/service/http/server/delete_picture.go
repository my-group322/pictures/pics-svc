package server

import (
	"net/http"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation"

	"github.com/go-chi/chi/v5"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

func (s Server) DeletePicture(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(validation.Errors{"id": err})...)
		return
	}

	if err := s.api.DeletePicture(r.Context(), id); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}
}
