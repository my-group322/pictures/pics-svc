package server

import (
	"encoding/json"
	"net/http"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/go-chi/chi/v5"
	"github.com/gorilla/schema"
)

func (s Server) GetChatHistory(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(validation.Errors{"id": err})...)
		return
	}

	var params resources.QueryParams
	_ = schema.NewDecoder().Decode(&params, r.URL.Query())

	response, err := s.api.GetChatHistory(r.Context(), params, id)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	_ = json.NewEncoder(w).Encode(response)
}
