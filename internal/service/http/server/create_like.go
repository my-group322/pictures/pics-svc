package server

import (
	"net/http"
	"strconv"

	validation "github.com/go-ozzo/ozzo-validation"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"

	"github.com/go-chi/chi/v5"
)

func (s Server) CreateLike(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		renderer.RenderErr(w, renderer.BadRequestErr(validation.Errors{"id": err})...)
		return
	}

	if err := s.api.CreateLike(r.Context(), id); err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}
}
