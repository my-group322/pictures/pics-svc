package server

import (
	"encoding/json"
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"
)

func (s Server) CreatePicture(w http.ResponseWriter, r *http.Request) {
	file, header, err := r.FormFile("picture")
	if err != nil {
		s.log.WithField("requester", ctx.Requester(r.Context())).WithError(err).Error("failed to form file")
		renderer.RenderStatus(w, http.StatusInternalServerError)
		return
	}

	response, err := s.api.CreatePicture(r.Context(), file, header)
	if err != nil {
		s.log.Error(err)
		renderer.RenderErr(w, renderer.NewJsonErr(err)...)
		return
	}

	_ = json.NewEncoder(w).Encode(response)
}
