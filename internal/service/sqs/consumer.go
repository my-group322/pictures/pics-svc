package sqs

import (
	"context"
	"encoding/json"

	"gitlab.com/my-group322/pictures/pics-svc/pkg/s3pictures"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"gitlab.com/my-group322/pictures/img-moderation-lambdas/lambda/moderator/resources"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/api"
)

type HandlersManager struct {
	log *logrus.Logger
	api api.API
}

func NewHandlersManager(log *logrus.Logger, api api.API) *HandlersManager {
	return &HandlersManager{log: log, api: api}
}

func (h HandlersManager) ImageModerationResultsConsumer(message []byte) error {
	var result resources.ModerationResult
	err := json.Unmarshal(message, &result)
	if err != nil {
		return errors.Wrap(err, "failed to unmarshal message")
	}

	pictureID, err := s3pictures.ParsePictureKeyToID(result.Picture.Key)
	if err != nil {
		return errors.Wrap(err, "failed to parse picture key")
	}

	return h.api.UpdatePictureSuspended(context.Background(), pictureID, true)
}
