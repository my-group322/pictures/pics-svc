package api

import (
	"context"
	"testing"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestApi_GetChatList(t *testing.T) {
	var (
		ctx            = context.Background()
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		chatsQClone    = &datamock.ChatsQCloner{}
		chatsQ         = &datamock.ChatsQ{}
		ownerID        = uuid.NewString()
		userID         = uuid.NewString()
		pictureID      = int64(1)
		chatID         = int64(1)
	)

	ctx = ctx2.RequesterToCtx(ownerID)(ctx)

	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)
	chatsQClone.On("Clone", mock.Anything).Return(chatsQ)

	api := New(&app.App{
		Log:       logrus.New(),
		PicturesQ: picturesQClone,
		ChatsQ:    chatsQClone,
	})

	t.Run("positive", func(t *testing.T) {
		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				ID:      pictureID,
				Owner:   ownerID,
				Privacy: data.PicturePrivacyPublic,
			},
		}, nil).Once()

		chatsQ.On("Select", pictureID).Return([]data.ChatFullInfo{{
			Chat: data.Chat{
				ID:        chatID,
				PictureID: pictureID,
			},
			ChattingWithDetails: data.User{
				ID:        userID,
				FirstName: "Illia",
				LastName:  "Syvoshapka",
				HasAvatar: false,
			},
		}}, nil).Once()

		messages, err := api.GetChatList(ctx, resources.QueryParams{FilterByPicture: pictureID})
		require.NoError(t, err)
		require.Len(t, messages.Data, 1)
	})

	t.Run("negative_no_filter_by_picture", func(t *testing.T) {
		_, err := api.GetChatList(ctx, resources.QueryParams{})
		require.Error(t, err)
	})

	t.Run("negative_picture_not_found", func(t *testing.T) {
		picturesQ.On("GetFullInfoByID", pictureID).Return(nil, nil).Once()

		_, err := api.GetChatList(ctx, resources.QueryParams{FilterByPicture: pictureID})
		require.ErrorIs(t, err, ErrPictureNotFound)
	})

	t.Run("negative_not_owner", func(t *testing.T) {
		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				ID:      pictureID,
				Owner:   ownerID,
				Privacy: data.PicturePrivacyToSelectedUsers,
			},
		}, nil).Once()

		ctx := ctx2.RequesterToCtx(userID)(context.Background())

		_, err := api.GetChatList(ctx, resources.QueryParams{FilterByPicture: pictureID})
		require.ErrorIs(t, err, ErrNoPicturePermission)
	})
}
