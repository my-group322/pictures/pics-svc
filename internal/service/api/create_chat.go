package api

import (
	"context"
	"errors"
	"strconv"

	jares "gitlab.com/my-group322/json-api-shared"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

func (a api) CreateChat(ctx context.Context, pictureID int64) (*resources.ChatResponse, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"pictureID": pictureID,
	})

	usersQ := a.UsersQ.Clone(ctx)
	picturesQ := a.PicturesQ.Clone(ctx)
	chatsQ := a.ChatsQ.Clone(ctx)

	ok, picture, err := checkPictureViewPermission(pictureID, usersQ, picturesQ, requester)
	if err != nil {
		log.WithError(err).Error("failed to check view permission")
		return nil, err
	}

	if !ok {
		return nil, ErrNoPicturePermission
	}

	if requester == picture.Owner {
		return nil, ErrSelfChat
	}

	chatID, err := chatsQ.Create(&data.Chat{
		PictureID:    pictureID,
		ChattingWith: requester,
	})
	if err != nil {
		log.WithError(err).Error("failed to create chat")

		if errors.Is(err, pkgdata.ErrUniqueViolationError) {
			return nil, ErrChatExists
		}

		return nil, err
	}

	return &resources.ChatResponse{
		Data: resources.Chat{
			Key: jares.Key{ID: strconv.FormatInt(chatID, 10), Type: resources.ResourceTypeChat},
		},
	}, nil
}
