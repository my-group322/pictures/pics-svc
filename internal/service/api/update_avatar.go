package api

import (
	"context"
	"mime/multipart"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	// Decoder requires them be imported like this to be recognized
	_ "image/jpeg"
	_ "image/png"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

const (
	maxAvatarSize   = 1 * 1000 * 1000 * 1000
	avatarsS3Folder = "pp/"
)

func (a api) UpdateAvatar(ctx context.Context, file multipart.File, header *multipart.FileHeader, userID string) (*resources.PictureResponse, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithField("requester", requester)

	usersQ := a.UsersQ.Clone(ctx)

	user, err := usersQ.GetByID(userID)
	if err != nil {
		log.WithError(err).Error("failed to get user")
		return nil, err
	}

	if user == nil {
		return nil, ErrUserNotFound
	}

	if requester != user.ID {
		return nil, ErrNoPicturePermission
	}

	if header.Size > maxAvatarSize {
		return nil, ErrPicTooBig
	}

	_, jpgImgBuf, _, err := validateImgAndMakeJpg(log, header, file)
	if err != nil {
		log.WithError(err).Error("failed to validate img and make jpg")
		return nil, err
	}

	result, err := a.S3Uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(a.Cfg.AwsS3BucketName),
		Key:    aws.String(avatarsS3Folder + userID + ".jpg"),
		Body:   jpgImgBuf,
		ACL:    aws.String("public-read"),
	})
	if err != nil {
		log.WithError(err).Error("failed to upload file")
		return nil, err
	}

	if !user.HasAvatar {
		if err := usersQ.UpdateHasAvatar(userID, true); err != nil {
			log.WithError(err).Error("failed to update has avatar")
			return nil, err
		}
	}

	return &resources.PictureResponse{
		Data: resources.Picture{
			Attributes: resources.PictureAttributes{
				Link: result.Location,
			},
		},
	}, nil
}
