package api

import (
	"context"
	"strconv"

	jares "gitlab.com/my-group322/json-api-shared"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/pkg/errors"
)

func (a api) GetChatList(ctx context.Context, params resources.QueryParams) (*resources.ChatListResponse, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"picChat":   params.FilterByPicture,
	})

	if params.FilterByPicture == 0 {
		return nil, validation.Errors{"filter[picture]": errors.New("should be present")}
	}

	picturesQ := a.PicturesQ.Clone(ctx)
	chatsQ := a.ChatsQ.Clone(ctx)

	picture, err := picturesQ.GetFullInfoByID(params.FilterByPicture)
	if err != nil {
		log.WithError(err).Error("failed to get picture")
		return nil, err
	}

	if picture == nil {
		return nil, ErrPictureNotFound
	}

	if picture.Owner != requester {
		return nil, ErrNoPicturePermission
	}

	chats, err := chatsQ.Select(picture.ID)
	if err != nil {
		log.WithError(err).Error("failed to select users")
		return nil, err
	}

	response := resources.ChatListResponse{Data: make([]resources.Chat, 0, len(chats))}
	for i := range chats {
		response.Data = append(response.Data, resources.Chat{
			Key: jares.Key{ID: strconv.FormatInt(chats[i].ID, 10), Type: resources.ResourceTypeChat},
			Relationships: resources.ChatRelationships{
				ChattingWith: *jares.Key{ID: chats[i].ChattingWith, Type: resources.ResourceTypeUser}.AsRelation(),
			},
		})

		chats[i].ChattingWithDetails.ID = chats[i].ChattingWith
		user := newUser(&chats[i].ChattingWithDetails)
		response.Included.Add(&user)
	}

	return &response, nil
}
