package api

import (
	"context"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

// GetPicsList used for multiple purposes. When user wants to see:
// 1) All their pictures. They pass filter by owner with their id.
// 2) All visible pictures from all owners. They ignore filter.
// 3) All visible pictures from some specific user. They pass this user ID as param.
func (a api) GetPicsList(ctx context.Context, params resources.QueryParams) ([]data.Picture, int64, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithField("requester", requester)

	picturesQ := a.PicturesQ.Clone(ctx)

	if params.FilterByOwner != "" {
		picturesQ.FilterByOwner(params.FilterByOwner)
	}

	var (
		pictures  []data.Picture
		picsCount int64
		err       error
	)

	if params.FilterByOwner == requester {
		pictures, picsCount, err = picturesQ.Select(params)
		if err != nil {
			log.WithError(err).Error("failed to get pictures")
			return nil, 0, err
		}
	} else {
		pictures, picsCount, err = picturesQ.SelectVisible(requester, params)
		if err != nil {
			log.WithError(err).Error("failed to get pictures")
			return nil, 0, err
		}
	}

	return pictures, picsCount, nil
}
