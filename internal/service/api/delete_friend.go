package api

import (
	"context"
	"errors"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"
)

func (a api) DeleteFriend(ctx context.Context, friendID string) error {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"friendID":  friendID,
	})

	usersQ := a.UsersQ.Clone(ctx)

	if err := usersQ.DeleteFriend(data.UsersFriends{UserID: requester, FriendUserID: friendID}); err != nil {
		log.WithError(err).Error("failed to add friend")

		if errors.Is(err, pkgdata.ErrNotAffected) {
			return ErrNotFriend
		}

		return err
	}

	return nil
}
