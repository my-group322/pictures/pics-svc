package api

import (
	"context"
	"testing"
	"time"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestApi_GetChatHistory(t *testing.T) {
	var (
		ctx            = context.Background()
		usersQClone    = &datamock.UsersQCloner{}
		usersQ         = &datamock.UsersQ{}
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		chatsQClone    = &datamock.ChatsQCloner{}
		chatsQ         = &datamock.ChatsQ{}
		ownerID        = uuid.NewString()
		userID         = uuid.NewString()
		pictureID      = int64(1)
		chatID         = int64(1)
	)

	ctx = ctx2.RequesterToCtx(userID)(ctx)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)
	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)
	chatsQClone.On("Clone", mock.Anything).Return(chatsQ)

	api := New(&app.App{
		Log:       logrus.New(),
		UsersQ:    usersQClone,
		PicturesQ: picturesQClone,
		ChatsQ:    chatsQClone,
	})

	t.Run("positive", func(t *testing.T) {
		chatsQ.On("GetByID", chatID).Return(&data.Chat{
			ID:        chatID,
			PictureID: pictureID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				Owner:   ownerID,
				Privacy: data.PicturePrivacyPublic,
			},
		}, nil).Once()

		chatsQ.On("GetChatHistory", chatID, mock.Anything).Return([]data.Message{{
			ID:               1,
			CreatedAt:        time.Now(),
			PictureOwnerSaid: false,
			Message:          "Hello",
		}}, nil).Once()

		messages, err := api.GetChatHistory(ctx, resources.QueryParams{}, chatID)
		require.NoError(t, err)
		require.Len(t, messages.Data, 1)
	})

	t.Run("negative_picture_not_found", func(t *testing.T) {
		chatsQ.On("GetByID", chatID).Return(&data.Chat{
			ID:        chatID,
			PictureID: pictureID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(nil, nil).Once()

		_, err := api.GetChatHistory(ctx, resources.QueryParams{}, chatID)
		require.ErrorIs(t, err, ErrPictureNotFound)
	})

	t.Run("negative_no_picture_permission", func(t *testing.T) {
		chatsQ.On("GetByID", chatID).Return(&data.Chat{
			ID:        chatID,
			PictureID: pictureID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				ID:      pictureID,
				Owner:   ownerID,
				Privacy: data.PicturePrivacyToSelectedUsers,
			},
		}, nil).Once()

		picturesQ.On("CheckViewPermission", chatID, userID).Return(false, nil)

		_, err := api.GetChatHistory(ctx, resources.QueryParams{}, chatID)
		require.ErrorIs(t, err, ErrNoPicturePermission)
	})

	t.Run("negative_chat_not_found", func(t *testing.T) {
		chatsQ.On("GetByID", chatID).Return(nil, nil).Once()

		_, err := api.GetChatHistory(ctx, resources.QueryParams{}, chatID)
		require.ErrorIs(t, err, ErrChatNotFound)
	})
}
