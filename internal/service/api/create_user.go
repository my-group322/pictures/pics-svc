package api

import (
	"context"
	"errors"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func (a api) CreateUser(ctx context.Context, user *resources.User) error {
	email := user.Attributes.Email
	userID := user.ID

	log := a.Log.WithFields(map[string]interface{}{
		"email":  email,
		"userID": userID,
	})

	usersQ := a.UsersQ.Clone(ctx)

	if err := usersQ.Create(&data.User{
		ID:        userID,
		Email:     email,
		FirstName: user.Attributes.FirstName,
		LastName:  user.Attributes.LastName,
	}); err != nil {
		log.WithError(err).Error("failed to create user")

		if errors.Is(err, pkgdata.ErrUniqueViolationError) {
			return ErrUserExists
		}

		return err
	}

	return nil
}
