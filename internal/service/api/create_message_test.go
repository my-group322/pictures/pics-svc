package api

import (
	"context"
	"testing"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/pkg/errors"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestApi_CreateMessage(t *testing.T) {
	var (
		ctx            = context.Background()
		usersQClone    = &datamock.UsersQCloner{}
		usersQ         = &datamock.UsersQ{}
		chatsQClone    = &datamock.ChatsQCloner{}
		chatsQ         = &datamock.ChatsQ{}
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		ownerID        = uuid.NewString()
		userID         = uuid.NewString()
		pictureID      = int64(1)
		message        = "Hi"
	)

	ctx = ctx2.RequesterToCtx(userID)(ctx)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)
	chatsQClone.On("Clone", mock.Anything).Return(chatsQ)
	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)

	api := New(&app.App{
		Log:       logrus.New(),
		UsersQ:    usersQClone,
		ChatsQ:    chatsQClone,
		PicturesQ: picturesQClone,
	})

	t.Run("positive_pic_owner_sends", func(t *testing.T) {
		req := &resources.SendMessage{ChatID: 1, Message: message}

		chatsQ.On("GetByID", req.ChatID).Return(&data.Chat{
			ID:           req.ChatID,
			PictureID:    pictureID,
			ChattingWith: userID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{Picture: data.Picture{Owner: ownerID}}, nil).Once()

		chatsQ.On("AddMessage", &data.Message{
			ChatID:           req.ChatID,
			PictureOwnerSaid: true,
			Message:          message,
		}).Return(nil).Once()

		ctx := ctx2.RequesterToCtx(ownerID)(context.Background())

		receiverID, err := api.CreateMessage(ctx, req)
		require.NoError(t, err)
		require.Equal(t, userID, receiverID)
	})

	t.Run("positive_not_owner_sends", func(t *testing.T) {
		req := &resources.SendMessage{ChatID: 1, Message: message}

		chatsQ.On("GetByID", req.ChatID).Return(&data.Chat{
			ID:           req.ChatID,
			PictureID:    pictureID,
			ChattingWith: userID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				Owner:   ownerID,
				Privacy: data.PicturePrivacyPublic,
			},
		}, nil).Once()

		chatsQ.On("AddMessage", &data.Message{
			ChatID:           req.ChatID,
			PictureOwnerSaid: false,
			Message:          message,
		}).Return(nil).Once()

		receiverID, err := api.CreateMessage(ctx, req)
		require.NoError(t, err)
		require.Equal(t, ownerID, receiverID)
	})

	t.Run("negative_random_user_sends_to_not_their_chat", func(t *testing.T) {
		req := &resources.SendMessage{ChatID: 1, Message: message}

		chatsQ.On("GetByID", req.ChatID).Return(&data.Chat{
			ID:           req.ChatID,
			PictureID:    pictureID,
			ChattingWith: userID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				Owner:   ownerID,
				Privacy: data.PicturePrivacyPublic,
			},
		}, nil).Once()

		ctx := ctx2.RequesterToCtx(uuid.NewString())(context.Background())

		_, err := api.CreateMessage(ctx, req)
		require.ErrorIs(t, err, ErrNoChatPermission)
	})

	t.Run("negative_no_picture_permission", func(t *testing.T) {
		req := &resources.SendMessage{ChatID: 1, Message: message}

		chatsQ.On("GetByID", req.ChatID).Return(&data.Chat{
			ID:           req.ChatID,
			PictureID:    pictureID,
			ChattingWith: userID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				Owner:   ownerID,
				Privacy: data.PicturePrivacyPublicToFriends,
			},
		}, nil).Once()

		usersQ.On("IsFriend", ownerID, userID).Return(false, nil)

		_, err := api.CreateMessage(ctx, req)
		require.ErrorIs(t, err, ErrNoPicturePermission)
	})

	t.Run("negative_chat_not_found", func(t *testing.T) {
		req := &resources.SendMessage{ChatID: 1, Message: message}

		chatsQ.On("GetByID", req.ChatID).Return(nil, nil).Once()

		_, err := api.CreateMessage(ctx, req)
		require.ErrorIs(t, err, ErrChatNotFound)
	})

	t.Run("negative_picture_not_found", func(t *testing.T) {
		req := &resources.SendMessage{ChatID: 1, Message: message}

		chatsQ.On("GetByID", req.ChatID).Return(&data.Chat{
			ID:           req.ChatID,
			PictureID:    pictureID,
			ChattingWith: userID,
		}, nil).Once()

		picturesQ.On("GetFullInfoByID", pictureID).Return(nil, nil).Once()

		_, err := api.CreateMessage(ctx, req)
		require.ErrorIs(t, err, ErrPictureNotFound)
	})

	t.Run("negative_db_error", func(t *testing.T) {
		req := &resources.SendMessage{ChatID: 1, Message: message}

		chatsQ.On("GetByID", req.ChatID).Return(nil, errors.New("")).Once()

		_, err := api.CreateMessage(ctx, req)
		require.Error(t, err)
	})
}
