package api

import (
	"context"
	"testing"
	"time"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestApi_GetPicsList(t *testing.T) {
	var (
		ctx            = context.Background()
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		ownerID        = uuid.NewString()
		userID         = uuid.NewString()
		pictureID      = int64(1)
	)

	ctx = ctx2.RequesterToCtx(userID)(ctx)

	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)

	api := New(&app.App{
		Log:       logrus.New(),
		PicturesQ: picturesQClone,
	})

	t.Run("positive_all_visible_pictures", func(t *testing.T) {
		picturesQ.On("SelectVisible", userID, mock.Anything).Return([]data.Picture{{
			ID:        pictureID,
			CreatedAt: time.Now(),
			Thumbnail: []byte("thumbnail"),
		}}, int64(1), nil)

		pictures, count, err := api.GetPicsList(ctx, resources.QueryParams{})
		require.NoError(t, err)
		require.Equal(t, int64(1), count)
		require.Len(t, pictures, 1)
	})

	t.Run("positive_all_visible_pictures_from_user", func(t *testing.T) {
		picturesQ.On("SelectVisible", userID, mock.Anything).Return([]data.Picture{{
			ID:        pictureID,
			CreatedAt: time.Now(),
			Thumbnail: []byte("thumbnail"),
		}}, int64(1), nil)

		picturesQ.On("FilterByOwner", ownerID)

		pictures, count, err := api.GetPicsList(ctx, resources.QueryParams{FilterByOwner: ownerID})
		require.NoError(t, err)
		require.Equal(t, int64(1), count)
		require.Len(t, pictures, 1)
	})

	t.Run("positive_owned_pictures", func(t *testing.T) {
		picturesQ.On("Select", mock.Anything).Return([]data.Picture{{
			ID:        pictureID,
			CreatedAt: time.Now(),
			Thumbnail: []byte("thumbnail"),
		}}, int64(1), nil)

		picturesQ.On("FilterByOwner", ownerID)

		ctx := ctx2.RequesterToCtx(ownerID)(context.Background())

		pictures, count, err := api.GetPicsList(ctx, resources.QueryParams{FilterByOwner: ownerID})
		require.NoError(t, err)
		require.Equal(t, int64(1), count)
		require.Len(t, pictures, 1)
	})
}
