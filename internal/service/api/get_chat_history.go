package api

import (
	"context"
	"strconv"

	jares "gitlab.com/my-group322/json-api-shared"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

func (a api) GetChatHistory(ctx context.Context, params resources.QueryParams, chatID int64) (*resources.MessageListResponse, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"chatID":    chatID,
	})

	chatsQ := a.ChatsQ.Clone(ctx)
	picturesQ := a.PicturesQ.Clone(ctx)
	usersQ := a.UsersQ.Clone(ctx)

	chat, err := chatsQ.GetByID(chatID)
	if err != nil {
		log.WithError(err).Error("failed to get picture")
		return nil, err
	}

	if chat == nil {
		return nil, ErrChatNotFound
	}

	ok, _, err := checkPictureViewPermission(chat.PictureID, usersQ, picturesQ, requester)
	if err != nil {
		log.WithError(err).Error("failed to check view permission")
		return nil, err
	}

	if !ok {
		return nil, ErrNoPicturePermission
	}

	messages, err := chatsQ.GetChatHistory(chat.ID, params)
	if err != nil {
		log.WithError(err).Error("failed to get chat history")
		return nil, err
	}

	response := resources.MessageListResponse{Data: make([]resources.Message, 0, len(messages))}
	for i := range messages {
		response.Data = append(response.Data, resources.Message{
			Key: jares.Key{ID: strconv.FormatInt(messages[i].ID, 10), Type: resources.ResourceTypeMessage},
			Attributes: resources.MessageAttributes{
				CreatedAt:        messages[i].CreatedAt,
				Message:          messages[i].Message,
				PictureOwnerSaid: messages[i].PictureOwnerSaid,
			},
		})
	}

	return &response, nil
}
