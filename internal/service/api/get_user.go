package api

import (
	"context"

	jares "gitlab.com/my-group322/json-api-shared"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

func (a api) GetUser(ctx context.Context, userID string) (*resources.UserResponse, error) {
	log := a.Log.WithField("userID", userID)

	usersQ := a.UsersQ.Clone(ctx)

	user, err := usersQ.GetByID(userID)
	if err != nil {
		log.WithError(err).Error("failed to get user")
		return nil, err
	}

	if user == nil {
		return nil, ErrUserNotFound
	}

	usersQ.FilterFriends(userID)
	friends, err := usersQ.SelectWithoutLimit()
	if err != nil {
		log.WithError(err).Error("failed to select friends")
		return nil, err
	}

	response := resources.UserResponse{Data: newUser(user)}

	for i := range friends {
		response.Data.Relationships.Friends.Data = append(response.Data.Relationships.Friends.Data,
			jares.Key{ID: friends[i].ID, Type: resources.ResourceTypeUser})
	}

	return &response, nil
}
