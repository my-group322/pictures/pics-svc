package api

import (
	"context"

	"github.com/pkg/errors"
)

func (a api) UpdatePictureSuspended(ctx context.Context, pictureID int64, isSuspended bool) error {
	picturesQ := a.PicturesQ.Clone(ctx)

	picture, err := picturesQ.GetByID(pictureID)
	if err != nil {
		return errors.Wrap(err, "failed to get picture")
	}

	picture.IsSuspended = isSuspended

	if err = picturesQ.Update(picture); err != nil {
		return errors.Wrap(err, "failed to update picture")
	}

	return nil
}
