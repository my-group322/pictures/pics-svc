package api

import (
	"context"
	"strconv"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"

	"github.com/pkg/errors"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func (a api) DeletePicture(ctx context.Context, pictureID int64) error {
	log := a.Log.WithField("pictureID", pictureID)

	picturesQ := a.PicturesQ.Clone(ctx)

	picture, err := picturesQ.GetByID(pictureID)
	if err != nil {
		log.WithError(err).Error("failed to get picture")
		return err
	}

	if picture == nil {
		return ErrPictureNotFound
	}

	if ctx2.Requester(ctx) != picture.Owner {
		return ErrNoPicturePermission
	}

	err = picturesQ.Transaction(func(tx data.PicturesQ) error {
		if err = tx.Delete(pictureID); err != nil {
			log.WithError(err).Error("failed to delete picture")

			if errors.Is(err, pkgdata.ErrNotAffected) {
				return ErrPictureNotFound
			}

			return err
		}

		_, err = a.S3.DeleteObject(&s3.DeleteObjectInput{
			Bucket: aws.String(a.Cfg.AwsS3BucketName),
			Key:    aws.String(strconv.Itoa(int(pictureID))),
		})
		if err != nil {
			log.WithError(err).Error("failed to delete s3 picture")
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}

	return nil
}
