package api

import (
	"bufio"
	"bytes"
	"context"
	"fmt"
	"image"
	_ "image/jpeg"
	_ "image/png"
	"mime/multipart"
	"strconv"

	jares "gitlab.com/my-group322/json-api-shared"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/ilyasiv2003/imgediting"
	"github.com/sirupsen/logrus"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

const (
	maxPictureSize   = 15 * 1000 * 1000 * 1000
	picturesS3Folder = "pics/"
)

func (a api) CreatePicture(ctx context.Context, file multipart.File, header *multipart.FileHeader) (*resources.PictureResponse, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithField("requester", requester)

	if header.Size > maxPictureSize {
		return nil, ErrPicTooBig
	}

	imgConfig, jpgImgBuf, img, err := validateImgAndMakeJpg(log, header, file)
	if err != nil {
		log.WithError(err).Error("failed to validate and make jpg")
		return nil, err
	}

	thumbnail, err := imgediting.MakeThumbnail(img, *imgConfig)
	if err != nil {
		log.WithError(err).Error("failed to make thumbnail")
		return nil, err
	}

	picturesQ := a.PicturesQ.Clone(ctx)

	var pictureID int64
	err = picturesQ.Transaction(func(tx data.PicturesQ) error {
		pictureID, err = tx.Create(&data.Picture{
			Thumbnail: thumbnail,
			Owner:     requester,
			Privacy:   data.PicturePrivacyToSelectedUsers,
		})
		if err != nil {
			return err
		}

		_, err = a.S3Uploader.Upload(&s3manager.UploadInput{
			Bucket: aws.String(a.Cfg.AwsS3BucketName),
			Key:    aws.String(fmt.Sprintf("%s%d%s", picturesS3Folder, pictureID, ".jpg")),
			Body:   jpgImgBuf,
		})
		if err != nil {
			log.WithError(err).Error("failed to upload file")
			return err
		}

		return nil
	})
	if err != nil {
		log.WithError(err).Error("tx failed")
		return nil, err
	}

	return &resources.PictureResponse{
		Data: resources.Picture{
			Key: jares.Key{ID: strconv.FormatInt(pictureID, 10), Type: resources.ResourceTypePicture},
		},
	}, nil
}

func validateImgAndMakeJpg(log *logrus.Entry, header *multipart.FileHeader, file multipart.File) (*image.Config, *bytes.Buffer, image.Image, error) {
	imgConfig, _, err := image.DecodeConfig(bufio.NewReader(file))
	if err != nil {
		return nil, nil, nil, ErrUnsupportedFormat
	}

	_, err = file.Seek(0, 0)
	if err != nil {
		log.WithError(err).Error("failed to seek file beginning")
		return nil, nil, nil, err
	}

	img, _, err := image.Decode(file)
	if err != nil {
		log.WithError(err).Error("failed to decode image")
		return nil, nil, nil, err
	}

	_, err = file.Seek(0, 0)
	if err != nil {
		log.WithError(err).Error("failed to seek file beginning")
		return nil, nil, nil, err
	}

	// Returned img is certainly jpg
	jpgImgBuf, img, err := imgediting.MakeJpgImage(file, header, img)
	if err != nil {
		log.WithError(err).Error("failed to make jpg image")
		return nil, nil, nil, err
	}

	return &imgConfig, jpgImgBuf, img, nil
}
