package api

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"
)

func TestApi_CreateLike(t *testing.T) {
	var (
		ctx            = context.Background()
		usersQClone    = &datamock.UsersQCloner{}
		usersQ         = &datamock.UsersQ{}
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		ownerID        = uuid.NewString()
		userID         = uuid.NewString()
		pictureID      = int64(1)
	)

	ctx = ctx2.RequesterToCtx(userID)(ctx)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)
	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)

	api := New(&app.App{
		Log:       logrus.New(),
		UsersQ:    usersQClone,
		PicturesQ: picturesQClone,
	})

	t.Run("positive", func(t *testing.T) {
		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				Owner:   ownerID,
				Privacy: data.PicturePrivacyPublic,
			},
		}, nil).Once()

		picturesQ.On("Like", mock.Anything).Return(nil).Once()

		err := api.CreateLike(ctx, pictureID)
		require.NoError(t, err)
	})

	t.Run("negative_picture_not_found", func(t *testing.T) {
		picturesQ.On("GetFullInfoByID", pictureID).Return(nil, nil).Once()

		err := api.CreateLike(ctx, pictureID)
		require.ErrorIs(t, err, ErrPictureNotFound)
	})

	t.Run("negative_no_picture_permission", func(t *testing.T) {
		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				ID:      pictureID,
				Owner:   ownerID,
				Privacy: data.PicturePrivacyToSelectedUsers,
			},
		}, nil).Once()

		picturesQ.On("CheckViewPermission", pictureID, userID).Return(false, nil)

		err := api.CreateLike(ctx, pictureID)
		require.ErrorIs(t, err, ErrNoPicturePermission)
	})

	t.Run("negative_like_conflict", func(t *testing.T) {
		picturesQ.On("GetFullInfoByID", pictureID).Return(&data.PictureFullInfo{
			Picture: data.Picture{
				Owner:   ownerID,
				Privacy: data.PicturePrivacyPublic,
			},
		}, nil).Once()

		picturesQ.On("Like", mock.Anything).Return(pkgdata.ErrUniqueViolationError).Once()

		err := api.CreateLike(ctx, pictureID)
		require.ErrorIs(t, err, ErrAlreadyLiked)
	})
}
