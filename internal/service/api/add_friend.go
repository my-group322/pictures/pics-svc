package api

import (
	"context"
	"errors"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func (a api) AddFriend(ctx context.Context, friendID string) error {
	requester := ctx2.Requester(ctx)

	if requester == friendID {
		return ErrSelfFriend
	}

	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"friendID":  friendID,
	})

	usersQ := a.UsersQ.Clone(ctx)

	if err := usersQ.AddFriend(data.UsersFriends{UserID: requester, FriendUserID: friendID}); err != nil {
		log.WithError(err).Error("failed to add friend")

		if errors.Is(err, pkgdata.ErrUniqueViolationError) {
			return ErrAlreadyFriend
		}

		return err
	}

	return nil
}
