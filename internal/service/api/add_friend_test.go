package api

import (
	"context"
	"errors"
	"testing"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
)

func TestApi_AddFriend(t *testing.T) {
	var (
		ctx         = context.Background()
		usersQClone = datamock.UsersQCloner{}
		usersQ      = &datamock.UsersQ{}
		friendID    = uuid.NewString()
		userID      = uuid.NewString()
	)

	ctx = ctx2.RequesterToCtx(userID)(ctx)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)
	api := New(&app.App{
		Log:    logrus.New(),
		UsersQ: &usersQClone,
	})

	t.Run("positive", func(t *testing.T) {
		usersQ.On("AddFriend", mock.Anything).Return(nil).Once()

		err := api.AddFriend(ctx, friendID)
		require.NoError(t, err)
	})

	t.Run("negative_self_friend", func(t *testing.T) {
		err := api.AddFriend(ctx, userID)
		require.ErrorIs(t, err, ErrSelfFriend)
	})

	t.Run("negative_already_friend", func(t *testing.T) {
		usersQ.On("AddFriend", mock.Anything).Return(pkgdata.ErrUniqueViolationError).Once()

		err := api.AddFriend(ctx, friendID)
		require.ErrorIs(t, err, ErrAlreadyFriend)
	})

	t.Run("negative_db_error", func(t *testing.T) {
		usersQ.On("AddFriend", mock.Anything).Return(errors.New("")).Once()

		err := api.AddFriend(ctx, friendID)
		require.Error(t, err)
	})
}
