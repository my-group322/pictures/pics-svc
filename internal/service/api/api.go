package api

import (
	"context"
	"mime/multipart"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

//go:generate mockery --case underscore --name API --inpackage

type API interface {
	AddFriend(ctx context.Context, friendID string) error
	CreateChat(ctx context.Context, pictureID int64) (*resources.ChatResponse, error)
	CreateLike(ctx context.Context, pictureID int64) error
	CreatePicture(ctx context.Context, file multipart.File, header *multipart.FileHeader) (*resources.PictureResponse, error)
	CreateUser(ctx context.Context, user *resources.User) error
	CreateMessage(ctx context.Context, request *resources.SendMessage) (chattingWith string, err error)
	DeleteFriend(ctx context.Context, friendID string) error
	DeleteLike(ctx context.Context, pictureID int64) error
	DeletePicture(ctx context.Context, pictureID int64) error
	GetChatHistory(ctx context.Context, params resources.QueryParams, chatID int64) (*resources.MessageListResponse, error)
	GetChatList(ctx context.Context, params resources.QueryParams) (*resources.ChatListResponse, error)
	GetPicsList(ctx context.Context, params resources.QueryParams) (pictures []data.Picture, total int64, err error)
	GetPicture(ctx context.Context, pictureID int64) (*resources.PictureResponse, error)
	GetUser(ctx context.Context, userID string) (*resources.UserResponse, error)
	GetUserList(ctx context.Context, params resources.QueryParams) (*resources.UserListResponse, error)
	UpdateAvatar(ctx context.Context, file multipart.File, header *multipart.FileHeader, userID string) (*resources.PictureResponse, error)
	UpdatePicture(ctx context.Context, pictureID int64, reqPicture *resources.Picture) error
	UpdatePictureSuspended(ctx context.Context, pictureID int64, isSuspended bool) error
	UpdateProfile(ctx context.Context, userID string, reqUser *resources.User) error
}

type api struct {
	*app.App
}

func New(app *app.App) API {
	return &api{app}
}

type Initiator int64

const (
	InitiatorUser Initiator = iota
	InitiatorSystem
)
