package api

import (
	"context"
	"errors"
	"testing"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"
	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"
)

func TestApi_DeleteFriend(t *testing.T) {
	var (
		ctx         = context.Background()
		usersQClone = datamock.UsersQCloner{}
		usersQ      = &datamock.UsersQ{}
		friendID    = uuid.NewString()
		userID      = uuid.NewString()
	)

	ctx = ctx2.RequesterToCtx(userID)(ctx)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)
	api := New(&app.App{
		Log:    logrus.New(),
		UsersQ: &usersQClone,
	})

	t.Run("positive", func(t *testing.T) {
		usersQ.On("DeleteFriend", mock.Anything).Return(nil).Once()

		err := api.DeleteFriend(ctx, friendID)
		require.NoError(t, err)
	})

	t.Run("negative_not_friend", func(t *testing.T) {
		usersQ.On("DeleteFriend", mock.Anything).Return(pkgdata.ErrNotAffected).Once()

		err := api.DeleteFriend(ctx, friendID)
		require.ErrorIs(t, err, ErrNotFriend)
	})

	t.Run("negative_db_error", func(t *testing.T) {
		usersQ.On("DeleteFriend", mock.Anything).Return(errors.New("")).Once()

		err := api.DeleteFriend(ctx, friendID)
		require.Error(t, err)
	})
}
