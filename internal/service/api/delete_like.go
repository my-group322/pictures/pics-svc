package api

import (
	"context"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"github.com/pkg/errors"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func (a api) DeleteLike(ctx context.Context, pictureID int64) error {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"pictureID": pictureID,
	})

	usersQ := a.UsersQ.Clone(ctx)
	picturesQ := a.PicturesQ.Clone(ctx)

	ok, _, err := checkPictureViewPermission(pictureID, usersQ, picturesQ, requester)
	if err != nil {
		log.WithError(err).Error("failed to check permission")
		return err
	}

	if !ok {
		return ErrNoPicturePermission
	}

	if err = picturesQ.Unlike(data.UsersLikedPics{
		PictureID: pictureID,
		UserID:    requester,
	}); err != nil {
		log.WithError(err).Error("failed to to like")

		if errors.Is(err, pkgdata.ErrNotAffected) {
			return ErrNotLiked
		}

		return err
	}

	return nil
}
