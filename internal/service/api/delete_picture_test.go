package api

import (
	"context"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app/mocks/s3mock"
	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/config"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestApi_DeletePicture(t *testing.T) {
	var (
		ctx            = context.Background()
		log            = logrus.New()
		picturesQClone = &datamock.PicturesQCloner{}
		picturesQ      = &datamock.PicturesQ{}
		ownerID        = uuid.NewString()
		s3             = s3mock.S3API{}
		pictureID      = int64(1)
	)

	ctx = ctx2.RequesterToCtx(ownerID)(ctx)

	s3.On("DeleteObject", mock.Anything).Return(nil, nil)
	picturesQClone.On("Clone", mock.Anything).Return(picturesQ)

	api := New(&app.App{
		S3:        &s3,
		Log:       log,
		PicturesQ: picturesQClone,
		Cfg:       &config.Config{AwsS3BucketName: "name"},
	})

	t.Run("positive_user_initiated", func(t *testing.T) {
		picturesQ.On("GetByID", pictureID).Return(&data.Picture{
			ID:      pictureID,
			Owner:   ownerID,
			Privacy: data.PicturePrivacyPublic,
		}, nil).Once()

		picturesQ.On("Delete", pictureID).Return(nil).Once()

		picturesQ.On("Transaction", mock.Anything).Run(func(args mock.Arguments) {
			txCallback, ok := args.Get(0).(func(tx data.PicturesQ) error)
			require.True(t, ok)

			err := txCallback(picturesQ)
			require.NoError(t, err)
		}).Return(nil).Once()

		err := api.DeletePicture(ctx, pictureID)
		require.NoError(t, err)
	})

	t.Run("negative_not_owner", func(t *testing.T) {
		picturesQ.On("GetByID", pictureID).Return(&data.Picture{
			ID:      pictureID,
			Owner:   uuid.NewString(),
			Privacy: data.PicturePrivacyPublic,
		}, nil).Once()

		err := api.DeletePicture(ctx, pictureID)
		require.ErrorIs(t, err, ErrNoPicturePermission)
	})

	t.Run("negative_picture_not_found", func(t *testing.T) {
		picturesQ.On("GetByID", pictureID).Return(nil, nil).Once()

		err := api.DeletePicture(ctx, pictureID)
		require.ErrorIs(t, err, ErrPictureNotFound)
	})
}
