package api

import (
	"context"
	"testing"

	"github.com/stretchr/testify/mock"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestApi_GetUser(t *testing.T) {
	var (
		ctx         = context.Background()
		usersQClone = &datamock.UsersQCloner{}
		usersQ      = &datamock.UsersQ{}
		userID      = uuid.NewString()
	)

	usersQClone.On("Clone", mock.Anything).Return(usersQ)

	api := New(&app.App{
		Log:    logrus.New(),
		UsersQ: usersQClone,
	})

	t.Run("positive", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(&data.User{
			ID:        userID,
			Email:     uuid.NewString(),
			FirstName: "Illia",
			LastName:  "Syvoshapka",
		}, nil).Once()

		usersQ.On("FilterFriends", userID)

		usersQ.On("SelectWithoutLimit").Return([]data.User{{
			ID: uuid.NewString(),
		}}, nil).Once()

		messages, err := api.GetUser(ctx, userID)
		require.NoError(t, err)
		require.Len(t, messages.Data.Relationships.Friends.Data, 1)
	})

	t.Run("negative_not_found", func(t *testing.T) {
		usersQ.On("GetByID", userID).Return(nil, nil).Once()

		_, err := api.GetUser(ctx, userID)
		require.ErrorIs(t, err, ErrUserNotFound)
	})
}
