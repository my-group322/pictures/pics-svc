package api

import (
	"net/http"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/renderer"
)

var (
	ErrAlreadyFriend       = renderer.Error{Message: "user is already friend", StatusCode: http.StatusForbidden}
	ErrNotFriend           = renderer.Error{Message: "user is not friend", StatusCode: http.StatusForbidden}
	ErrSelfFriend          = renderer.Error{Message: "user can't be friend with themselves", StatusCode: http.StatusForbidden}
	ErrPictureNotFound     = renderer.Error{Message: "picture not found", StatusCode: http.StatusNotFound}
	ErrUserNotFound        = renderer.Error{Message: "user not found", StatusCode: http.StatusNotFound}
	ErrUserNotVerified     = renderer.Error{Message: "user not verified", StatusCode: http.StatusForbidden}
	ErrSelfChat            = renderer.Error{Message: "user can't  chat with themselves", StatusCode: http.StatusForbidden}
	ErrNotProfileOwner     = renderer.Error{Message: "user should be profile owner", StatusCode: http.StatusForbidden}
	ErrChatExists          = renderer.Error{Message: "chat already exists", StatusCode: http.StatusConflict}
	ErrChatNotFound        = renderer.Error{Message: "chat not found", StatusCode: http.StatusNotFound}
	ErrAlreadyLiked        = renderer.Error{Message: "already liked", StatusCode: http.StatusForbidden}
	ErrNotLiked            = renderer.Error{Message: "not liked", StatusCode: http.StatusForbidden}
	ErrPicTooBig           = renderer.Error{Message: "picture size should be less than 15 Mb (avatar < 1 Mb)", StatusCode: http.StatusForbidden}
	ErrUnsupportedFormat   = renderer.Error{Message: "picture should be either png or jpeg(jpg)", StatusCode: http.StatusForbidden}
	ErrUserExists          = renderer.Error{Message: "user already exists", StatusCode: http.StatusConflict}
	ErrNoPicturePermission = renderer.Error{Message: "user don't have permission to picture", StatusCode: http.StatusForbidden}
	ErrNoChatPermission    = renderer.Error{Message: "user don't have permission to chat", StatusCode: http.StatusForbidden}
)
