package api

import (
	"context"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"
)

func (a api) UpdateProfile(ctx context.Context, userID string, reqUser *resources.User) error {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithField("requester", requester)

	if userID != requester {
		return ErrNotProfileOwner
	}

	usersQ := a.UsersQ.Clone(ctx)

	user, err := usersQ.GetByID(userID)
	if err != nil {
		log.WithError(err).Error("failed to get user")
		return err
	}

	if user == nil {
		return ErrUserNotFound
	}

	user.FirstName = reqUser.Attributes.FirstName
	user.LastName = reqUser.Attributes.LastName

	if err = usersQ.Update(user); err != nil {
		log.WithError(err).Error("failed to update user")
		return err
	}

	return nil
}
