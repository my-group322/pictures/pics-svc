package api

import (
	"context"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

func (a api) GetUserList(ctx context.Context, params resources.QueryParams) (*resources.UserListResponse, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithField("requester", requester)

	usersQ := a.UsersQ.Clone(ctx)

	if params.FilterByAllNames != "" {
		usersQ.FilterByAnyName(params.FilterByAllNames)
	}

	if params.FilterByFirstName != "" {
		usersQ.FilterByFirstName(params.FilterByFirstName)
	}

	if params.FilterByLastName != "" {
		usersQ.FilterByLastName(params.FilterByLastName)
	}

	if params.FilterFriends {
		usersQ.FilterFriends(requester)
	}

	users, err := usersQ.Select(params)
	if err != nil {
		log.WithError(err).Error("failed to select users")
		return nil, err
	}

	var response resources.UserListResponse
	response.Data = make([]resources.User, 0, len(users))

	for i := range users {
		response.Data = append(response.Data, newUser(&users[i]))
	}

	return &response, nil
}
