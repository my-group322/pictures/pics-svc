package api

import (
	"context"
	"errors"
	"sort"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	validation "github.com/go-ozzo/ozzo-validation"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func (a api) UpdatePicture(ctx context.Context, pictureID int64, reqPicture *resources.Picture) error {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithField("requester", requester)

	picturesQ := a.PicturesQ.Clone(ctx)

	picture, err := picturesQ.GetByID(pictureID)
	if err != nil {
		log.WithError(err).Error("failed to get picture")
		return err
	}

	if picture == nil {
		return ErrPictureNotFound
	}

	if picture.Owner != requester {
		return ErrNoPicturePermission
	}

	var pr data.PicturePrivacy
	privacyTo, ok := pr.Parse(reqPicture.Attributes.Privacy)
	if !ok {
		return validation.Errors{"data/attributes/privacy": errors.New("invalid privacy")}
	}

	if privacyTo != data.PicturePrivacyToSelectedUsers && len(reqPicture.Relationships.SharedWith.Data) > 0 {
		return validation.Errors{"data/relationships/shared_with": errors.New("expected empty sharedWith when privacy is not to selected")}
	}

	permittedUsers, err := picturesQ.GetPermittedUsers(picture.ID)
	if err != nil {
		log.WithError(err).Error("failed to get permitted users")
		return err
	}

	newPermittedUsers := make([]string, 0)
	for _, item := range reqPicture.Relationships.SharedWith.Data {
		newPermittedUsers = append(newPermittedUsers, item.ID)
	}

	if !equalSlices(permittedUsers, newPermittedUsers) {
		if err := picturesQ.UpdateViewPermissions(picture.ID, newPermittedUsers); err != nil {
			log.WithError(err).Error("failed to update picture")
			return err
		}
	}

	picture.Privacy = privacyTo
	picture.Comment = reqPicture.Attributes.Comment

	if err = picturesQ.Update(picture); err != nil {
		log.WithError(err).Error("failed to update picture")
		return err
	}

	return nil
}

func equalSlices(users1 []string, users2 []string) bool {
	if len(users1) != len(users2) {
		return false
	}

	sort.Slice(users1, func(i, j int) bool {
		return users1[i] < users1[j]
	})

	sort.Slice(users2, func(i, j int) bool {
		return users2[i] < users2[j]
	})

	for i := range users1 {
		if users1[i] != users2[i] {
			return false
		}
	}

	return true
}
