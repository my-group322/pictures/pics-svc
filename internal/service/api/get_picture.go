package api

import (
	"context"
	"fmt"
	"strconv"
	"time"

	jares "gitlab.com/my-group322/json-api-shared"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/pkg/errors"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func (a api) GetPicture(ctx context.Context, pictureID int64) (*resources.PictureResponse, error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"pictureID": pictureID,
	})

	picturesQ := a.PicturesQ.Clone(ctx)
	usersQ := a.UsersQ.Clone(ctx)
	chatsQ := a.ChatsQ.Clone(ctx)

	ok, picture, err := checkPictureViewPermission(pictureID, usersQ, picturesQ, requester)
	if err != nil {
		log.WithError(err).Error("failed to check view permission")
		return nil, err
	}

	if !ok {
		return nil, ErrNoPicturePermission
	}

	req, _ := a.S3.GetObjectRequest(&s3.GetObjectInput{
		Bucket: aws.String(a.Cfg.AwsS3BucketName),
		Key:    aws.String(fmt.Sprintf("%s%d.jpg", picturesS3Folder, picture.ID)),
	})

	filePath, err := req.Presign(15 * time.Minute)
	if err != nil {
		log.WithError(err).Error("failed to sign url")
		return nil, err
	}

	isLiked, err := picturesQ.IsLiked(requester, picture.ID)
	if err != nil {
		log.WithError(err).Error("failed to get is liked")
		return nil, err
	}

	response := resources.PictureResponse{
		Data: resources.Picture{
			Key: jares.Key{ID: strconv.FormatInt(picture.ID, 10), Type: resources.ResourceTypePicture},
			Attributes: resources.PictureAttributes{
				Link:      filePath,
				CreatedAt: picture.CreatedAt,
				Privacy:   picture.Privacy.Int64(),
				Likes:     picture.Likes,
				IsLiked:   isLiked,
				Comment:   picture.Comment,
			},
			Relationships: resources.PictureRelationships{
				Owner: *jares.Key{ID: picture.Owner, Type: resources.ResourceTypeUser}.AsRelation(),
				SharedWith: jares.RelationCollection{
					Data: make([]jares.Key, 0),
				},
			},
		},
	}

	if requester != picture.Owner {
		chat, err := chatsQ.GetByChattingWithAndPicID(requester, picture.ID)
		if err != nil {
			log.WithError(err).Error("failed to get chat")
			return nil, err
		}

		if chat != nil {
			response.Data.Relationships.Chat = *jares.Key{ID: strconv.FormatInt(chat.ID, 10), Type: resources.ResourceTypeChat}.AsRelation()
		}
	}

	if picture.Privacy == data.PicturePrivacyToSelectedUsers {
		usersQ.FilterByID(picture.SharedWith)
		usersSharedWith, err := usersQ.SelectWithoutLimit()
		if err != nil {
			log.WithError(err).Error("failed to select users shared with")
			return nil, err
		}

		for i := range picture.SharedWith {
			response.Data.Relationships.SharedWith.Data = append(response.Data.Relationships.SharedWith.Data,
				jares.Key{ID: picture.SharedWith[i], Type: resources.ResourceTypeUser})

			user := newUser(&usersSharedWith[i])
			response.Included.Add(&user)
		}
	}

	owner := newUser(&picture.OwnerDetails)
	response.Included.Add(&owner)

	return &response, nil
}

func checkPictureViewPermission(pictureID int64, usersQ data.UsersQ, picturesQ data.PicturesQ, requester string) (bool, *data.PictureFullInfo, error) {
	picture, err := picturesQ.GetFullInfoByID(pictureID)
	if err != nil {
		return false, nil, errors.Wrap(err, "failed to get picture")
	}

	if picture == nil || (picture.Owner != requester && picture.IsSuspended) {
		return false, nil, ErrPictureNotFound
	}

	if picture.Owner == requester {
		return true, picture, nil
	}

	switch picture.Privacy {
	case data.PicturePrivacyPublic:
		return true, picture, nil

	case data.PicturePrivacyPublicToFriends:
		ok, err := usersQ.IsFriend(picture.Owner, requester)
		return ok, picture, err

	case data.PicturePrivacyToSelectedUsers:
		ok, err := picturesQ.CheckViewPermission(picture.ID, requester)
		return ok, picture, err

	default:
		return false, nil, nil
	}
}

func newUser(user *data.User) resources.User {
	return resources.User{
		Key: jares.Key{ID: user.ID, Type: resources.ResourceTypeUser},
		Attributes: resources.UserAttributes{
			FirstName: user.FirstName,
			LastName:  user.LastName,
			HasAvatar: user.HasAvatar,
		},
	}
}
