package api

import (
	"context"
	"testing"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	datamock "gitlab.com/my-group322/pictures/pics-svc/internal/data/mocks"
)

func TestApi_GetUserList(t *testing.T) {
	var (
		ctx         = context.Background()
		usersQClone = &datamock.UsersQCloner{}
		usersQ      = &datamock.UsersQ{}
		userID      = uuid.NewString()
	)

	ctx = ctx2.RequesterToCtx(userID)(context.Background())

	usersQClone.On("Clone", mock.Anything).Return(usersQ)

	api := New(&app.App{
		Log:    logrus.New(),
		UsersQ: usersQClone,
	})

	t.Run("positive_all_filters", func(t *testing.T) {
		usersQ.On("FilterByAnyName", mock.Anything)
		usersQ.On("FilterByFirstName", mock.Anything)
		usersQ.On("FilterByLastName", mock.Anything)
		usersQ.On("FilterFriends", mock.Anything)

		usersQ.On("Select", mock.Anything).Return([]data.User{{
			ID:        uuid.NewString(),
			FirstName: "Illia",
			LastName:  "Syvoshapka",
			HasAvatar: false,
		}}, nil).Once()

		messages, err := api.GetUserList(ctx, resources.QueryParams{
			FilterByAllNames:  "a",
			FilterByFirstName: "a",
			FilterByLastName:  "a",
			FilterFriends:     true,
		})
		require.NoError(t, err)
		require.Len(t, messages.Data, 1)
	})
}
