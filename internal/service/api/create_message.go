package api

import (
	"context"

	ctx2 "gitlab.com/my-group322/pictures/pics-svc/internal/service/ctx"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

func (a api) CreateMessage(ctx context.Context, message *resources.SendMessage) (chattingWith string, err error) {
	requester := ctx2.Requester(ctx)
	log := a.Log.WithFields(map[string]interface{}{
		"requester": requester,
		"chatID":    message.ChatID,
	})

	chatsQ := a.ChatsQ.Clone(context.Background())
	picturesQ := a.PicturesQ.Clone(context.Background())
	usersQ := a.UsersQ.Clone(context.Background())

	chat, err := chatsQ.GetByID(message.ChatID)
	if err != nil {
		log.WithError(err).Error("failed to get chat")
		return "", err
	}

	if chat == nil {
		log.Error("chat not found")
		return "", ErrChatNotFound
	}

	// This rules out the case when person no longer has view permission for picture due to privacy change
	ok, picture, err := checkPictureViewPermission(chat.PictureID, usersQ, picturesQ, requester)
	if err != nil {
		log.WithError(err).Error("failed to check picture view permission")
		return "", err
	}

	if !ok {
		return "", ErrNoPicturePermission
	}

	if requester != picture.Owner && requester != chat.ChattingWith {
		log.Error("don't have permission to send message")
		return "", ErrNoChatPermission
	}

	if err := chatsQ.AddMessage(&data.Message{
		ChatID:           message.ChatID,
		PictureOwnerSaid: picture.Owner == requester,
		Message:          message.Message,
	}); err != nil {
		log.WithError(err).Error("failed to create message")
		return "", err
	}

	receiverID := chat.ChattingWith
	if requester != picture.Owner {
		receiverID = picture.Owner
	}

	return receiverID, nil
}
