package app

import (
	"strings"

	"github.com/ilyasiv2003/gosqs"

	"github.com/assembla/cony"
	credentialsv2 "github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3iface"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface"
	redislib "github.com/go-redis/redis/v8"
	"github.com/ilyasiv2003/newrelic-context/nrgorm"
	newrelic "github.com/newrelic/go-agent"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	driver "gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/my-group322/pictures/pics-svc/internal/config"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data/postgres"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data/redis"
	"gitlab.com/my-group322/pictures/pics-svc/internal/rabbitmq"
)

//go:generate mockery --case underscore --name UploaderAPI --srcpkg github.com/aws/aws-sdk-go/service/s3/s3manager/s3manageriface --output mocks/s3mock --outpkg s3mock
//go:generate mockery --case underscore --name S3API --srcpkg github.com/aws/aws-sdk-go/service/s3/s3iface --output mocks/s3mock --outpkg s3mock
//go:generate mockery --case underscore --name Client --srcpkg github.com/ilyasiv2003/gosqs --output mocks/sqsmock --outpkg sqsmock

// App holds all main dependencies that have to be initialized on start
type App struct {
	Log           *logrus.Logger
	Cfg           *config.Config
	DB            *gorm.DB
	S3            s3iface.S3API
	S3Uploader    s3manageriface.UploaderAPI
	Notifications *cony.Publisher
	Redis         *redislib.Client
	RedisQ        data.RedisQCloner
	PicturesQ     data.PicturesQCloner
	UsersQ        data.UsersQCloner
	ChatsQ        data.ChatsQCloner
	Metrics       newrelic.Application
	Sqs           gosqs.Client
}

func New(cfg *config.Config) *App {
	var err error

	a := &App{
		Log: logrus.New(),
		Cfg: cfg,
	}

	a.DB, err = gorm.Open(driver.Open(cfg.DBUrl), &gorm.Config{SkipDefaultTransaction: true})
	if err != nil {
		panic(errors.Wrap(err, "failed to open database"))
	}
	nrgorm.AddGormCallbacks(a.DB)

	a.UsersQ = postgres.NewUsersQCloner(a.DB)
	a.PicturesQ = postgres.NewPicturesQCloner(a.DB)
	a.ChatsQ = postgres.NewChatsQCloner(a.DB)

	awsSess, err := session.NewSession(&aws.Config{
		Region: &cfg.AwsRegion,
		Credentials: credentials.NewStaticCredentials(
			cfg.AwsAccessKeyID,
			cfg.AwsSecretKey,
			"",
		),
	})
	if err != nil {
		panic(errors.Wrap(err, "failed to create aws session"))
	}

	a.S3 = s3.New(awsSess)
	a.S3Uploader = s3manager.NewUploader(awsSess)

	a.Notifications = rabbitmq.Notifications(cfg.RabbitMQUrl, a.Log)

	a.Redis = redislib.NewClient(&redislib.Options{
		Addr:     cfg.RedisURL,
		Password: "",
		DB:       0,
	})

	a.RedisQ = redis.NewRedisQ(a.Redis)

	if a.Cfg.NewRelicLicenceKey == "" {
		a.Log.Warn("metrics are disabled")

		// Disabling app with invalid key
		a.Cfg.NewRelicLicenceKey = strings.Repeat("a", 40)
	}
	a.Metrics, err = newrelic.NewApplication(newrelic.NewConfig("pics-svc", cfg.NewRelicLicenceKey))
	if err != nil {
		panic(errors.Wrap(err, "failed to setup metrics"))
	}

	a.Sqs = gosqs.NewClient(sqs.New(sqs.Options{
		Credentials: credentialsv2.NewStaticCredentialsProvider(
			cfg.AwsAccessKeyID,
			cfg.AwsSecretKey,
			"",
		),
		Region: cfg.AwsRegion,
	}), a.Log)

	return a
}
