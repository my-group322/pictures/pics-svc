package service

import (
	"context"
	"net"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app/mocks/sqsmock"

	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/stretchr/testify/require"

	"github.com/sirupsen/logrus"
	"gitlab.com/my-group322/pictures/pics-svc/internal/config"
)

func TestService_Run(t *testing.T) {
	log := logrus.New()
	port := "2281"

	sqsMock := sqsmock.Client{}
	sqsMock.On("StartConsumers", mock.Anything).Return(nil)

	a := &app.App{
		Log: log,
		Cfg: &config.Config{Port: port},
		Sqs: &sqsMock,
	}

	svc := NewService(a)

	t.Run("negative_port_in_use", func(t *testing.T) {
		listener, err := net.Listen("tcp", "0.0.0.0:"+port)
		require.NoError(t, err)

		go func() {
			err = http.Serve(listener, http.NotFoundHandler())
			require.NoError(t, err)
		}()

		err = tryRun(svc, time.Millisecond*500)
		require.Error(t, err)
	})
}

func tryRun(service *Service, duration time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), duration)
	defer cancel()

	return service.Run(ctx)
}
