//go:build functional
// +build functional

package service

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	jares "gitlab.com/my-group322/json-api-shared"

	credentialsv2 "github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/sqs"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"

	imresources "gitlab.com/my-group322/pictures/img-moderation-lambdas/lambda/moderator/resources"
	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/gobwas/ws/wsutil"
	migrate "github.com/rubenv/sql-migrate"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/constants"
	"gitlab.com/my-group322/pictures/pics-svc/internal/service/app"

	"github.com/gobwas/ws"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"

	client "github.com/bozd4g/go-http-client"

	"github.com/google/uuid"

	"github.com/dgrijalva/jwt-go"
	jwt2 "gitlab.com/my-group322/pictures/auth-svc/pkg/jwt"

	"github.com/stretchr/testify/require"

	"github.com/pkg/errors"

	redislib "github.com/go-redis/redis/v8"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data/migrations"

	"github.com/sirupsen/logrus"
	"gitlab.com/my-group322/pictures/pics-svc/internal/config"
)

const testPictureLocation = "../../assets/sample_picture.jpg"

var (
	testServerURL  string
	testHTTPClient *httpClient
	redisClient    *redislib.Client
	a              *app.App
)

// There should be only 1 instance of this, which is testHTTPClient
type httpClient struct {
	cli client.Client
}

func (c httpClient) Do(req *http.Request, token string) (client.Response, error) {
	req.Header.Set("Cookie", fmt.Sprintf("%s=%s", "jwt-auth", token))
	return c.cli.Do(req)
}

func TestMain(m *testing.M) {
	os.Exit(setup(m))
}

// This func is necessary because of os.Exit and defer conflicts
func setup(m *testing.M) int {
	log := logrus.New()

	cfg, err := config.New(log)
	if err != nil {
		panic(errors.Wrap(err, "failed to setup config"))
	}

	testServerURL = "http://localhost:" + cfg.Port

	a = app.New(cfg)

	redisClient = a.Redis

	rawDB, _ := a.DB.DB()
	defer func() {
		if err = rawDB.Close(); err != nil {
			log.WithError(err).Fatalf("failed to close db")
		}
	}()

	if err = migrations.Migrate(log, a.DB, migrate.Up); err != nil {
		panic(errors.Wrap(err, "failed to apply migrations"))
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go func() {
		err := NewService(a).Run(ctx)
		if err != nil {
			panic(err)
		}
	}()

	if !waitForServerStart() {
		panic("server didn't start in reasonable time")
	}

	testHTTPClient = &httpClient{client.New(testServerURL)}

	return m.Run()
}

func waitForServerStart() bool {
	start := time.Now()
	for !ping() {
		if time.Since(start).Seconds() > 10 {
			return false
		}
		time.Sleep(time.Second)
	}
	return true
}

func ping() bool {
	resp, err := http.Get(testServerURL + "/ping")
	if err != nil || resp.StatusCode != http.StatusOK {
		return false
	}
	return true
}

// As a user I want to set different privacy options for my pictures, hence restrict access for other users.
func TestPicturePrivacy(t *testing.T) {
	user, err := newTestUser()
	require.NoError(t, err)

	pictureID := user.UploadPicture(t, testPictureLocation)

	t.Run("privacy_to_selected_no_permission", func(t *testing.T) {
		user.UpdatePicPrivacy(t, pictureID, data.PicturePrivacyToSelectedUsers)

		notLegitUser, err := newTestUser()
		require.NoError(t, err)

		notLegitUser.GetPictureAssertStatusCode(t, pictureID, http.StatusForbidden)
	})

	t.Run("privacy_to_selected_yes_permission", func(t *testing.T) {
		legitUser, err := newTestUser()
		require.NoError(t, err)

		user.UpdatePicPrivacy(t, pictureID, data.PicturePrivacyToSelectedUsers, legitUser.ID)

		legitUser.GetPictureAssertStatusCode(t, pictureID, http.StatusOK)
	})

	t.Run("privacy_public", func(t *testing.T) {
		user.UpdatePicPrivacy(t, pictureID, data.PicturePrivacyPublic)

		randomUser, err := newTestUser()
		require.NoError(t, err)

		randomUser.GetPictureAssertStatusCode(t, pictureID, http.StatusOK)
	})

	t.Run("privacy_to_friends_no_permission", func(t *testing.T) {
		user.UpdatePicPrivacy(t, pictureID, data.PicturePrivacyPublicToFriends)

		notFriendlyUser, err := newTestUser()
		require.NoError(t, err)

		notFriendlyUser.GetPictureAssertStatusCode(t, pictureID, http.StatusForbidden)
	})

	t.Run("privacy_to_friends_yes_permission", func(t *testing.T) {
		user.UpdatePicPrivacy(t, pictureID, data.PicturePrivacyPublicToFriends)

		friendlyUser, err := newTestUser()
		require.NoError(t, err)

		user.AddToFriends(t, friendlyUser.ID)

		friendlyUser.GetPicture(t, pictureID)
	})
}

// As a user I want to be able to delete my pictures
func TestDeletePicture(t *testing.T) {
	user, err := newTestUser()
	require.NoError(t, err)

	pictureID := user.UploadPicture(t, testPictureLocation)
	user.DeletePicture(t, pictureID)
	user.GetPictureAssertStatusCode(t, pictureID, http.StatusNotFound)

	_, err = a.S3.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(a.Cfg.AwsS3BucketName),
		Key:    aws.String(a.Cfg.AwsS3BucketName),
	})
	aErr, ok := err.(awserr.Error)
	require.True(t, ok)
	require.Equal(t, s3.ErrCodeNoSuchKey, aErr.Code())
}

// As a user I want to be able to like pictures and see up-to-date number of them.
func TestLikes(t *testing.T) {
	user, err := newTestUser()
	require.NoError(t, err)

	pictureID := user.UploadPicture(t, testPictureLocation)
	user.UpdatePicPrivacy(t, pictureID, data.PicturePrivacyPublic)

	anotherUser, err := newTestUser()
	require.NoError(t, err)

	anotherUser.ChangeLikePicture(t, pictureID, likeActionLike)

	picture := user.GetPicture(t, pictureID)
	require.Equal(t, int64(1), picture.Attributes.Likes)

	anotherUser.ChangeLikePicture(t, pictureID, likeActionUnlike)

	picture = user.GetPicture(t, pictureID)
	require.Equal(t, int64(0), picture.Attributes.Likes)
}

// As a picture owner I want to see new chats from people who can view my picture and to be able to chat with them.
// As a user I want to chat with picture owners and see chat history.
func TestChat(t *testing.T) {
	pictureOwner, err := newTestUser()
	require.NoError(t, err)

	pictureID := pictureOwner.UploadPicture(t, testPictureLocation)
	pictureOwner.UpdatePicPrivacy(t, pictureID, data.PicturePrivacyPublic)

	randomUser, err := newTestUser()
	require.NoError(t, err)

	chatID := randomUser.CreateChat(t, pictureID)

	ownerChatConn := pictureOwner.CreateChatConn(t)
	randomUserChatConn := randomUser.CreateChatConn(t)

	chatIDInt, _ := strconv.ParseInt(chatID, 10, 64)

	messageText := "Hello, 世界"
	message, err := json.Marshal(resources.SendMessage{
		ChatID:  chatIDInt,
		Message: messageText,
	})
	require.NoError(t, err)

	doneChan := make(chan struct{})

	go func() {
		msgRaw, err := wsutil.ReadServerText(ownerChatConn)
		require.NoError(t, err)

		var msg resources.SendMessage
		err = json.Unmarshal(msgRaw, &msg)
		require.NoError(t, err)
		require.Equal(t, messageText, msg.Message)

		doneChan <- struct{}{}
	}()

	err = wsutil.WriteClientMessage(randomUserChatConn, ws.OpText, message)
	require.NoError(t, err)

	<-doneChan

	messages := pictureOwner.GetChatHistory(t, chatID)
	require.Len(t, messages, 1)
	require.Equal(t, false, messages[0].Attributes.PictureOwnerSaid)
	require.Equal(t, messageText, messages[0].Attributes.Message)
}

// As a platform owner, I want to suspend pictures that are considered inappropriate by automated image moderation.
// Suspended pictures can be seen by owner but not other members.
func TestImageModeration(t *testing.T) {
	sqsCli := sqs.New(sqs.Options{
		Credentials: credentialsv2.NewStaticCredentialsProvider(
			a.Cfg.AwsAccessKeyID,
			a.Cfg.AwsSecretKey,
			"",
		),
		Region: a.Cfg.AwsRegion,
	})

	imgModerationUrlOut, err := sqsCli.GetQueueUrl(context.Background(), &sqs.GetQueueUrlInput{QueueName: aws.String(a.Cfg.ImageModerationQueue)})
	require.NoError(t, err)

	user, err := newTestUser()
	require.NoError(t, err)

	suspiciousPictureID := user.UploadPicture(t, testPictureLocation)
	user.UpdatePicPrivacy(t, suspiciousPictureID, data.PicturePrivacyPublic)

	payload := imresources.ModerationResult{
		Picture: imresources.Picture{
			Bucket: a.Cfg.AwsS3BucketName,
			Key:    fmt.Sprintf("pics/%s.jpg", suspiciousPictureID),
		},
		Outcome: imresources.ModerationOutcomeFail,
	}

	payloadRaw, err := json.Marshal(&payload)
	require.NoError(t, err)

	_, err = sqsCli.SendMessage(context.Background(), &sqs.SendMessageInput{
		QueueUrl:    imgModerationUrlOut.QueueUrl,
		MessageBody: aws.String(string(payloadRaw)),
	})
	require.NoError(t, err)

	time.Sleep(5 * time.Second)

	randomUser, err := newTestUser()
	require.NoError(t, err)

	randomUser.GetPictureAssertStatusCode(t, suspiciousPictureID, http.StatusNotFound)
	user.GetPicture(t, suspiciousPictureID)
}

type testUser struct {
	ID    string
	Token string
}

// newTestUser creates everything needed to make requests
func newTestUser() (*testUser, error) {
	userID := uuid.NewString()

	userRaw := &resources.UserResponse{
		Data: resources.User{
			Key: jares.Key{ID: userID, Type: resources.ResourceTypeUser},
			Attributes: resources.UserAttributes{
				Email:     uuid.NewString(),
				FirstName: "Ilya",
				LastName:  "Syvoshapka",
			},
		},
	}

	user, _ := json.Marshal(userRaw)

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s%s", testServerURL, "/users"), bytes.NewBuffer(user))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create request")
	}

	req.Header.Set("Authorization", a.Cfg.InternalAccessKey)

	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to do request")
	}
	resp.Body.Close()

	err = redisClient.HSet(context.TODO(), fmt.Sprintf("%s:%s", constants.RedisUsersKey, userID), constants.RedisLastPassChangeKey, "", constants.RedisIsVerifiedKey, "1").Err()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create identity")
	}

	token, err := newTokenAndSession(*a.Cfg, userRaw.Data.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create token and session")
	}

	return &testUser{
		ID:    userRaw.Data.ID,
		Token: token,
	}, nil
}

func (u testUser) MakeRequest(req *http.Request) (client.Response, error) {
	return testHTTPClient.Do(req, u.Token)
}

func newTokenAndSession(cfg config.Config, userID string) (string, error) {
	newSessID := redisClient.Incr(context.Background(), constants.RedisLastActiveSession).Val()
	err := redisClient.SAdd(context.Background(), constants.RedisSessionsKey, newSessID).Err()
	if err != nil {
		return "", errors.Wrap(err, "failed to create session")
	}

	claims := &jwt2.CustomClaimsJwt{
		UserID:         userID,
		SessionID:      newSessID,
		StandardClaims: jwt.StandardClaims{IssuedAt: time.Now().Unix()},
	}
	token, err := jwt.NewWithClaims(jwt.SigningMethodHS256, claims).SignedString([]byte(cfg.JwtSecret))
	if err != nil {
		return "", errors.Wrap(err, "failed to create token")
	}

	return token, nil
}

func (u testUser) UpdatePicPrivacy(t *testing.T, pictureID string, newPrivacy data.PicturePrivacy, permittedUsers ...string) {
	permittedUsersKeys := make([]jares.Key, 0, len(permittedUsers))
	for _, userID := range permittedUsers {
		permittedUsersKeys = append(permittedUsersKeys, jares.Key{ID: userID, Type: resources.ResourceTypeUser})
	}

	updatePic, err := json.Marshal(&resources.PictureResponse{
		Data: resources.Picture{
			Key:        jares.Key{ID: pictureID, Type: resources.ResourceTypePicture},
			Attributes: resources.PictureAttributes{Privacy: newPrivacy.Int64()},
			Relationships: resources.PictureRelationships{
				SharedWith: jares.RelationCollection{
					Data: permittedUsersKeys,
				},
			},
		},
	})
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPatch, fmt.Sprintf("%s/%s/%s", testServerURL, "pictures", pictureID), bytes.NewBuffer(updatePic))
	require.NoError(t, err)

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)
}

func (u testUser) AddToFriends(t *testing.T, newFriendID string) {
	addFriend, err := json.Marshal(&resources.UserResponse{
		Data: resources.User{
			Key: jares.Key{ID: newFriendID, Type: resources.ResourceTypeUser},
		},
	})
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s/%s/%s", testServerURL, "users", u.ID, "friends"), bytes.NewBuffer(addFriend))
	require.NoError(t, err)

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)
}

func (u testUser) UploadPicture(t *testing.T, picturePath string) string {
	picture, err := os.Open(picturePath)
	require.NoError(t, err)

	defer picture.Close()

	formBuf := bytes.NewBuffer(nil)
	w := multipart.NewWriter(formBuf)
	formWriter, err := w.CreateFormFile("picture", picture.Name())
	require.NoError(t, err)

	_, err = io.Copy(formWriter, picture)
	require.NoError(t, err)

	err = w.Close()
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s%s", testServerURL, "/pictures"), formBuf)
	require.NoError(t, err)

	// Required header for form data requests
	req.Header.Add("Content-Type", w.FormDataContentType())

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)

	result := &resources.PictureResponse{}
	err = json.Unmarshal(resp.Get().Body, &result)
	require.NoError(t, err)

	return result.Data.ID
}

type likeAction int

const (
	likeActionLike likeAction = iota
	likeActionUnlike
)

func (u testUser) ChangeLikePicture(t *testing.T, pictureID string, action likeAction) {
	method := http.MethodPost
	if action == likeActionUnlike {
		method = http.MethodDelete
	}

	req, err := http.NewRequest(method, fmt.Sprintf("%s/%s/%s/%s", testServerURL, "pictures", pictureID, "like"), nil)
	require.NoError(t, err)

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)
}

func (u testUser) GetPicture(t *testing.T, pictureID string) *resources.Picture {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/%s/%s", testServerURL, "pictures", pictureID), nil)
	require.NoError(t, err)

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)

	var picture resources.PictureResponse
	err = json.Unmarshal(resp.Get().Body, &picture)
	require.NoError(t, err)

	return &picture.Data
}

func (u testUser) GetPictureAssertStatusCode(t *testing.T, pictureID string, statusCode int) {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/%s/%s", testServerURL, "pictures", pictureID), nil)
	require.NoError(t, err)

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, statusCode, resp.Get().StatusCode)
}

func (u testUser) DeletePicture(t *testing.T, pictureID string) {
	req, err := http.NewRequest(http.MethodDelete, fmt.Sprintf("%s/pictures/%s", testServerURL, pictureID), nil)
	require.NoError(t, err)

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)
}

func (u testUser) CreateChat(t *testing.T, pictureID string) string {
	createChatReq, err := json.Marshal(&resources.PictureResponse{
		Data: resources.Picture{
			Key: jares.Key{ID: pictureID, Type: resources.ResourceTypePicture},
		},
	})
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, fmt.Sprintf("%s/%s", testServerURL, "chats"), bytes.NewBuffer(createChatReq))
	require.NoError(t, err)

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)

	var chatRes resources.ChatResponse
	err = json.Unmarshal(resp.Get().Body, &chatRes)
	require.NoError(t, err)

	return chatRes.Data.ID
}

func (u testUser) CreateChatConn(t *testing.T) net.Conn {
	dialer := ws.DefaultDialer
	dialer.Header = ws.HandshakeHeaderHTTP(http.Header{
		"Cookie": []string{fmt.Sprintf("%s=%s", "jwt-auth", u.Token)},
	})

	addrWithoutSchema := strings.TrimPrefix(testServerURL, "http://")

	conn, _, _, err := dialer.Dial(context.Background(), fmt.Sprintf("%s%s/%s", "ws://", addrWithoutSchema, "chat"))
	require.NoError(t, err)

	return conn
}

func (u testUser) GetChatHistory(t *testing.T, chatID string) []resources.Message {
	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/%s/%s", testServerURL, "chats", chatID), nil)
	require.NoError(t, err)

	q := req.URL.Query()
	q.Add("before", strconv.Itoa(1000000))

	req.URL.RawQuery = q.Encode()

	resp, err := u.MakeRequest(req)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, resp.Get().StatusCode)

	var messages resources.MessageListResponse
	err = json.Unmarshal(resp.Get().Body, &messages)
	require.NoError(t, err)

	return messages.Data
}
