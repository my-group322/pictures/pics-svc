package data

import "context"

type RedisQ interface {
	IsSessionPresent(sessionID int64) (bool, error)
	DeleteSession(sessionID int64) error
	GetIdentity(userID string) (*Identity, error)
}

type RedisQCloner interface {
	Clone(ctx context.Context) RedisQ
}

type Identity struct {
	IsVerified         bool
	LastPasswordChange *int64
}
