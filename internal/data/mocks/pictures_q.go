// Code generated by mockery v2.9.4. DO NOT EDIT.

package datamock

import (
	mock "github.com/stretchr/testify/mock"
	data "gitlab.com/my-group322/pictures/pics-svc/internal/data"

	resources "gitlab.com/my-group322/pictures/pics-svc/resources"
)

// PicturesQ is an autogenerated mock type for the PicturesQ type
type PicturesQ struct {
	mock.Mock
}

// CheckViewPermission provides a mock function with given fields: picID, userID
func (_m *PicturesQ) CheckViewPermission(picID int64, userID string) (bool, error) {
	ret := _m.Called(picID, userID)

	var r0 bool
	if rf, ok := ret.Get(0).(func(int64, string) bool); ok {
		r0 = rf(picID, userID)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int64, string) error); ok {
		r1 = rf(picID, userID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Create provides a mock function with given fields: picture
func (_m *PicturesQ) Create(picture *data.Picture) (int64, error) {
	ret := _m.Called(picture)

	var r0 int64
	if rf, ok := ret.Get(0).(func(*data.Picture) int64); ok {
		r0 = rf(picture)
	} else {
		r0 = ret.Get(0).(int64)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(*data.Picture) error); ok {
		r1 = rf(picture)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Delete provides a mock function with given fields: pictureID
func (_m *PicturesQ) Delete(pictureID int64) error {
	ret := _m.Called(pictureID)

	var r0 error
	if rf, ok := ret.Get(0).(func(int64) error); ok {
		r0 = rf(pictureID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// FilterByOwner provides a mock function with given fields: id
func (_m *PicturesQ) FilterByOwner(id string) {
	_m.Called(id)
}

// GetByID provides a mock function with given fields: id
func (_m *PicturesQ) GetByID(id int64) (*data.Picture, error) {
	ret := _m.Called(id)

	var r0 *data.Picture
	if rf, ok := ret.Get(0).(func(int64) *data.Picture); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*data.Picture)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int64) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetFullInfoByID provides a mock function with given fields: id
func (_m *PicturesQ) GetFullInfoByID(id int64) (*data.PictureFullInfo, error) {
	ret := _m.Called(id)

	var r0 *data.PictureFullInfo
	if rf, ok := ret.Get(0).(func(int64) *data.PictureFullInfo); ok {
		r0 = rf(id)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*data.PictureFullInfo)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int64) error); ok {
		r1 = rf(id)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetPermittedUsers provides a mock function with given fields: picID
func (_m *PicturesQ) GetPermittedUsers(picID int64) ([]string, error) {
	ret := _m.Called(picID)

	var r0 []string
	if rf, ok := ret.Get(0).(func(int64) []string); ok {
		r0 = rf(picID)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]string)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(int64) error); ok {
		r1 = rf(picID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// IsLiked provides a mock function with given fields: userID, pictureID
func (_m *PicturesQ) IsLiked(userID string, pictureID int64) (bool, error) {
	ret := _m.Called(userID, pictureID)

	var r0 bool
	if rf, ok := ret.Get(0).(func(string, int64) bool); ok {
		r0 = rf(userID, pictureID)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(string, int64) error); ok {
		r1 = rf(userID, pictureID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Like provides a mock function with given fields: relation
func (_m *PicturesQ) Like(relation data.UsersLikedPics) error {
	ret := _m.Called(relation)

	var r0 error
	if rf, ok := ret.Get(0).(func(data.UsersLikedPics) error); ok {
		r0 = rf(relation)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Select provides a mock function with given fields: params
func (_m *PicturesQ) Select(params resources.QueryParams) ([]data.Picture, int64, error) {
	ret := _m.Called(params)

	var r0 []data.Picture
	if rf, ok := ret.Get(0).(func(resources.QueryParams) []data.Picture); ok {
		r0 = rf(params)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]data.Picture)
		}
	}

	var r1 int64
	if rf, ok := ret.Get(1).(func(resources.QueryParams) int64); ok {
		r1 = rf(params)
	} else {
		r1 = ret.Get(1).(int64)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(resources.QueryParams) error); ok {
		r2 = rf(params)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// SelectVisible provides a mock function with given fields: userID, params
func (_m *PicturesQ) SelectVisible(userID string, params resources.QueryParams) ([]data.Picture, int64, error) {
	ret := _m.Called(userID, params)

	var r0 []data.Picture
	if rf, ok := ret.Get(0).(func(string, resources.QueryParams) []data.Picture); ok {
		r0 = rf(userID, params)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]data.Picture)
		}
	}

	var r1 int64
	if rf, ok := ret.Get(1).(func(string, resources.QueryParams) int64); ok {
		r1 = rf(userID, params)
	} else {
		r1 = ret.Get(1).(int64)
	}

	var r2 error
	if rf, ok := ret.Get(2).(func(string, resources.QueryParams) error); ok {
		r2 = rf(userID, params)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// Transaction provides a mock function with given fields: fn
func (_m *PicturesQ) Transaction(fn func(data.PicturesQ) error) error {
	ret := _m.Called(fn)

	var r0 error
	if rf, ok := ret.Get(0).(func(func(data.PicturesQ) error) error); ok {
		r0 = rf(fn)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Unlike provides a mock function with given fields: relation
func (_m *PicturesQ) Unlike(relation data.UsersLikedPics) error {
	ret := _m.Called(relation)

	var r0 error
	if rf, ok := ret.Get(0).(func(data.UsersLikedPics) error); ok {
		r0 = rf(relation)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// Update provides a mock function with given fields: picture
func (_m *PicturesQ) Update(picture *data.Picture) error {
	ret := _m.Called(picture)

	var r0 error
	if rf, ok := ret.Get(0).(func(*data.Picture) error); ok {
		r0 = rf(picture)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// UpdateViewPermissions provides a mock function with given fields: picID, users
func (_m *PicturesQ) UpdateViewPermissions(picID int64, users []string) error {
	ret := _m.Called(picID, users)

	var r0 error
	if rf, ok := ret.Get(0).(func(int64, []string) error); ok {
		r0 = rf(picID, users)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
