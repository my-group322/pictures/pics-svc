package data

import (
	"context"
	"time"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/lib/pq"
)

//go:generate mockery --case underscore --name "PicturesQ|PicturesQCloner" --outpkg datamock

type PicturesQ interface {
	Transaction(fn func(tx PicturesQ) error) error
	Create(picture *Picture) (int64, error)
	Update(picture *Picture) error
	Delete(pictureID int64) error
	Select(params resources.QueryParams) ([]Picture, int64, error)

	SelectVisible(userID string, params resources.QueryParams) ([]Picture, int64, error)

	IsLiked(userID string, pictureID int64) (bool, error)
	Like(relation UsersLikedPics) error
	Unlike(relation UsersLikedPics) error

	GetByID(id int64) (*Picture, error)
	GetFullInfoByID(id int64) (*PictureFullInfo, error)

	CheckViewPermission(picID int64, userID string) (bool, error)
	GetPermittedUsers(picID int64) ([]string, error)
	UpdateViewPermissions(picID int64, users []string) error

	FilterByOwner(id string)
}

type PicturesQCloner interface {
	Clone(ctx context.Context) PicturesQ
}

type Picture struct {
	ID          int64
	Thumbnail   []byte
	Owner       string
	CreatedAt   time.Time
	Privacy     PicturePrivacy
	Comment     string
	IsSuspended bool
}

type PictureFullInfo struct {
	Picture
	Likes        int64
	OwnerDetails User           `gorm:"embedded"`
	SharedWith   pq.StringArray `gorm:"type:integer[]"`
}

type UsersLikedPics struct {
	UserID    string
	PictureID int64
}

type PicturePrivacy string

const (
	PicturePrivacyPublic          PicturePrivacy = "PUBLIC"
	PicturePrivacyPublicToFriends PicturePrivacy = "TO_FRIENDS"
	PicturePrivacyToSelectedUsers PicturePrivacy = "TO_SELECTED"
)

var picturePrivacyFromInt = map[int64]PicturePrivacy{
	0: PicturePrivacyPublic,
	1: PicturePrivacyPublicToFriends,
	2: PicturePrivacyToSelectedUsers,
}

func (p *PicturePrivacy) Parse(value int64) (PicturePrivacy, bool) {
	e, ok := picturePrivacyFromInt[value]
	return e, ok
}

func (p PicturePrivacy) Int64() int64 {
	for k, v := range picturePrivacyFromInt {
		if v == p {
			return k
		}
	}
	return -1
}
