package data

import (
	"context"
	"time"

	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

//go:generate mockery --case underscore --name "ChatsQ|ChatsQCloner" --outpkg datamock

type ChatsQ interface {
	Create(chat *Chat) (int64, error)
	Select(pictureID int64) ([]ChatFullInfo, error)

	AddMessage(message *Message) error

	GetByID(chatID int64) (*Chat, error)
	GetByChattingWithAndPicID(userID string, picID int64) (*Chat, error)
	GetChatHistory(chatID int64, params resources.QueryParams) ([]Message, error)
}

type ChatsQCloner interface {
	Clone(ctx context.Context) ChatsQ
}

// Chat should be viewed from picture owner perspective, so chatting with is not owner
type Chat struct {
	ID           int64
	PictureID    int64
	ChattingWith string
}

type ChatFullInfo struct {
	Chat
	ChattingWithDetails User `gorm:"embedded"`
}

type Message struct {
	ID               int64
	ChatID           int64
	PictureOwnerSaid bool
	CreatedAt        time.Time
	Message          string
}
