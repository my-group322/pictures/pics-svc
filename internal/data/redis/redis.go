package redis

import (
	"context"
	"fmt"
	"strconv"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"github.com/go-redis/redis/v8"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/constants"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

type redisQ struct {
	*redis.Client
}

func NewRedisQ(cli *redis.Client) data.RedisQCloner {
	return redisQ{cli}
}

func (q redisQ) IsSessionPresent(sessionID int64) (bool, error) {
	return q.SIsMember(context.Background(), constants.RedisSessionsKey, sessionID).Result()
}

func (q redisQ) GetIdentity(userID string) (*data.Identity, error) {
	res, err := q.HGetAll(context.TODO(), q.identityRedisKey(userID)).Result()
	if err != nil {
		return nil, err
	}

	if len(res) == 0 {
		return nil, nil
	}

	isVerified, err := strconv.ParseBool(res[constants.RedisIsVerifiedKey])
	if err != nil {
		return nil, err
	}

	lastPassChange, err := q.parseLastPassChange(res[constants.RedisLastPassChangeKey])
	if err != nil {
		return nil, err
	}

	return &data.Identity{
		IsVerified:         isVerified,
		LastPasswordChange: lastPassChange,
	}, nil
}

func (q redisQ) DeleteSession(sessionID int64) error {
	return q.SRem(context.Background(), constants.RedisSessionsKey, sessionID).Err()
}

func (q redisQ) identityRedisKey(accountID string) string {
	return fmt.Sprintf("%s:%s", constants.RedisUsersKey, accountID)
}

func (q redisQ) parseLastPassChange(raw string) (*int64, error) {
	if raw == "" {
		return nil, nil
	}

	lastPassChange, err := strconv.ParseInt(raw, 10, 64)
	if err != nil {
		return nil, err
	}

	return &lastPassChange, nil
}

func (q redisQ) Clone(ctx context.Context) data.RedisQ {
	return redisQ{Client: nrcontext.WrapRedisClient(ctx, q.Client)}
}
