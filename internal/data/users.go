package data

import (
	"context"

	"gitlab.com/my-group322/pictures/pics-svc/resources"
)

//go:generate mockery --case underscore --name "UsersQ|UsersQCloner" --outpkg datamock

type UsersQ interface {
	Create(user *User) error
	Update(user *User) error
	Select(params resources.QueryParams) ([]User, error)
	SelectWithoutLimit() ([]User, error)

	GetByID(id string) (user *User, err error)

	FilterByFirstName(name string)
	FilterByLastName(name string)
	FilterByAnyName(name string)
	FilterFriends(userID string)
	FilterByID(ids []string)

	AddFriend(relation UsersFriends) error
	DeleteFriend(relation UsersFriends) error
	IsFriend(userID, friendID string) (bool, error)

	UpdateHasAvatar(id string, hasAvatar bool) error
}

type UsersQCloner interface {
	Clone(ctx context.Context) UsersQ
}

type User struct {
	ID        string
	Email     string
	FirstName string
	LastName  string
	HasAvatar bool
}

type UsersFriends struct {
	UserID       string
	FriendUserID string
}
