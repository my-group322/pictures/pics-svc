//go:build integration
// +build integration

package postgres

import (
	"context"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/google/uuid"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"

	"github.com/stretchr/testify/require"
)

func TestUsersQ_Update(t *testing.T) {
	user := createUser(t)

	newName := "New name"
	err := q.UsersQ.Update(&data.User{
		ID:        user.ID,
		FirstName: newName,
	})
	require.NoError(t, err)

	newUser, err := q.UsersQ.GetByID(user.ID)
	require.NoError(t, err)
	require.Equal(t, newName, newUser.FirstName)
}

func TestUsersQ_Select(t *testing.T) {
	user := &data.User{
		ID:        uuid.NewString(),
		Email:     uuid.NewString(),
		FirstName: "@(#",
		LastName:  "$)$",
	}

	err := q.UsersQ.Create(user)
	require.NoError(t, err)

	t.Run("positive_filter_by_names", func(t *testing.T) {
		usersQ := q.UsersQ.Clone(context.Background())

		usersQ.FilterByFirstName("(")
		usersQ.FilterByLastName(")")
		usersQ.FilterByAnyName("#")

		users, err := usersQ.Select(resources.QueryParams{})
		require.NoError(t, err)
		require.Len(t, users, 1)
		require.Equal(t, user.ID, users[0].ID)
	})

	t.Run("positive_filter_by_friends", func(t *testing.T) {
		friend := createUser(t)
		err = q.UsersQ.AddFriend(data.UsersFriends{UserID: user.ID, FriendUserID: friend.ID})
		require.NoError(t, err)

		usersQ := q.UsersQ.Clone(context.Background())
		usersQ.FilterFriends(user.ID)

		users, err := usersQ.Select(resources.QueryParams{})
		require.NoError(t, err)
		require.Len(t, users, 1)
		require.Equal(t, users[0].ID, friend.ID)
	})
}

func TestUsersQ_DeleteFriend(t *testing.T) {
	user := createUser(t)
	friend := createUser(t)

	err := q.UsersQ.AddFriend(data.UsersFriends{UserID: user.ID, FriendUserID: friend.ID})
	require.NoError(t, err)

	err = q.UsersQ.DeleteFriend(data.UsersFriends{UserID: user.ID, FriendUserID: friend.ID})
	require.NoError(t, err)

	ok, err := q.UsersQ.IsFriend(user.ID, friend.ID)
	require.NoError(t, err)
	require.False(t, ok)
}

func TestUsersQ_GetByID(t *testing.T) {
	t.Run("positive", func(t *testing.T) {
		user := createUser(t)

		actualUser, err := q.UsersQ.GetByID(user.ID)
		require.NoError(t, err)
		require.Equal(t, user.Email, actualUser.Email)
	})

	t.Run("positive_not_found", func(t *testing.T) {
		user, err := q.UsersQ.GetByID(uuid.NewString())
		require.NoError(t, err)
		require.Nil(t, user)
	})
}
