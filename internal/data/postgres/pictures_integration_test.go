//go:build integration
// +build integration

package postgres

import (
	"context"
	"testing"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	"github.com/google/uuid"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
)

func TestPicturesQ_Create(t *testing.T) {
	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newPicturesQ(notAvailableDB).Create(&data.Picture{})
		require.Error(t, err)
	})
}

func TestPicturesQ_Select(t *testing.T) {
	t.Run("positive_filtered_by_owner", func(t *testing.T) {
		user := createUser(t)
		createPicture(t, user.ID, data.PicturePrivacyPublic, false)
		createPicture(t, user.ID, data.PicturePrivacyPublic, false)
		createPicture(t, user.ID, data.PicturePrivacyPublic, true)

		picturesQ := q.PicturesQ.Clone(context.Background())

		picturesQ.FilterByOwner(user.ID)
		pics, count, err := picturesQ.Select(resources.QueryParams{})
		require.NoError(t, err)
		require.Len(t, pics, 3)

		require.Equal(t, int64(3), count)

		t.Run("empty_result", func(t *testing.T) {
			picturesQ.FilterByOwner(uuid.NewString())
			pics, count, err = picturesQ.Select(resources.QueryParams{})
			require.NoError(t, err)
			require.Empty(t, pics)

			require.Equal(t, int64(0), count)
		})
	})

	t.Run("negative_dial_err", func(t *testing.T) {
		_, _, err := newPicturesQ(notAvailableDB).Select(resources.QueryParams{})
		require.Error(t, err)
	})
}

func TestPicturesQ_SelectVisible(t *testing.T) {
	t.Run("positive", func(t *testing.T) {
		picOwnerID := createUser(t).ID

		publicPicID := createPicture(t, picOwnerID, data.PicturePrivacyPublic, false)
		onlyToFriendsPicID := createPicture(t, picOwnerID, data.PicturePrivacyPublicToFriends, false)
		onlySharedPicID := createPicture(t, picOwnerID, data.PicturePrivacyToSelectedUsers, false)

		randomUser := createUser(t)
		friendlyUser := createUser(t)
		userWithSelectedPermission := createUser(t)

		err := q.UsersQ.AddFriend(data.UsersFriends{
			UserID:       picOwnerID,
			FriendUserID: friendlyUser.ID,
		})
		require.NoError(t, err)

		err = q.PicturesQ.UpdateViewPermissions(onlySharedPicID, []string{userWithSelectedPermission.ID})
		require.NoError(t, err)

		picsHaveSpecifiedID := func(pics []data.Picture, ids ...int64) bool {
			for _, pic := range pics {
				for _, id := range ids {
					if pic.ID == id {
						return true
					}
				}
			}
			return false
		}

		t.Run("privacy_public", func(t *testing.T) {
			pics, count, err := q.PicturesQ.SelectVisible(randomUser.ID, resources.QueryParams{Size: 100})
			require.NoError(t, err)

			require.True(t, picsHaveSpecifiedID(pics, publicPicID))
			require.False(t, picsHaveSpecifiedID(pics, onlyToFriendsPicID))
			require.False(t, picsHaveSpecifiedID(pics, onlySharedPicID))

			require.Equal(t, len(pics), int(count))
		})

		t.Run("privacy_to_friends", func(t *testing.T) {
			pics, count, err := q.PicturesQ.SelectVisible(friendlyUser.ID, resources.QueryParams{Size: 100})
			require.NoError(t, err)

			require.True(t, picsHaveSpecifiedID(pics, publicPicID))
			require.True(t, picsHaveSpecifiedID(pics, onlyToFriendsPicID))
			require.False(t, picsHaveSpecifiedID(pics, onlySharedPicID))

			require.Equal(t, len(pics), int(count))
		})

		t.Run("privacy_to_selected", func(t *testing.T) {
			pics, count, err := q.PicturesQ.SelectVisible(userWithSelectedPermission.ID, resources.QueryParams{Size: 100})
			require.NoError(t, err)

			require.True(t, picsHaveSpecifiedID(pics, publicPicID))
			require.False(t, picsHaveSpecifiedID(pics, onlyToFriendsPicID))
			require.True(t, picsHaveSpecifiedID(pics, onlySharedPicID))

			require.Equal(t, len(pics), int(count))
		})

		t.Run("suspended_picture", func(t *testing.T) {
			publicPicID := createPicture(t, picOwnerID, data.PicturePrivacyPublic, true)
			onlyToFriendsPicID := createPicture(t, picOwnerID, data.PicturePrivacyPublicToFriends, true)
			onlySharedPicID := createPicture(t, picOwnerID, data.PicturePrivacyToSelectedUsers, true)

			assertAllNotVisible := func(userID string) {
				pics, _, err := q.PicturesQ.SelectVisible(randomUser.ID, resources.QueryParams{Size: 100})
				require.NoError(t, err)
				require.False(t, picsHaveSpecifiedID(pics, publicPicID, onlyToFriendsPicID, onlySharedPicID))
			}

			assertAllNotVisible(randomUser.ID)
			assertAllNotVisible(friendlyUser.ID)
			assertAllNotVisible(userWithSelectedPermission.ID)
		})
	})

	t.Run("negative_dial_err", func(t *testing.T) {
		_, _, err := newPicturesQ(notAvailableDB).SelectVisible("", resources.QueryParams{})
		require.Error(t, err)
	})
}

func TestPicturesQ_IsLiked(t *testing.T) {
	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newPicturesQ(notAvailableDB).IsLiked("", 0)
		require.Error(t, err)
	})
}

func TestPicturesQ_GetByID(t *testing.T) {
	t.Run("negative_not_found", func(t *testing.T) {
		pic, err := q.PicturesQ.GetByID(-1)
		require.NoError(t, err)
		require.Nil(t, pic)
	})

	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newPicturesQ(notAvailableDB).GetByID(0)
		require.Error(t, err)
	})
}

func TestPicturesQ_GetFullInfoByID(t *testing.T) {
	t.Run("negative_not_found", func(t *testing.T) {
		pic, err := q.PicturesQ.GetFullInfoByID(-1)
		require.NoError(t, err)
		require.Nil(t, pic)
	})

	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newPicturesQ(notAvailableDB).GetFullInfoByID(0)
		require.Error(t, err)
	})
}

func TestPicturesQ_CheckViewPermission(t *testing.T) {
	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newPicturesQ(notAvailableDB).CheckViewPermission(0, "")
		require.Error(t, err)
	})
}

func TestPicturesQ_GetPermittedUsers(t *testing.T) {
	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newPicturesQ(notAvailableDB).GetPermittedUsers(0)
		require.Error(t, err)
	})
}

func TestPicturesQ_UpdateViewPermissions(t *testing.T) {
	t.Run("negative_dial_err", func(t *testing.T) {
		err := newPicturesQ(notAvailableDB).UpdateViewPermissions(0, nil)
		require.Error(t, err)
	})
}
