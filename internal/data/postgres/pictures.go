package postgres

import (
	"context"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
	"gorm.io/gorm"
)

type picturesQ struct {
	db      *gorm.DB
	filters map[string]interface{}
}

func newPicturesQ(db *gorm.DB) *picturesQ {
	return &picturesQ{
		db:      db,
		filters: make(map[string]interface{}),
	}
}

func NewPicturesQCloner(db *gorm.DB) data.PicturesQCloner {
	return newPicturesQ(db)
}

func (q *picturesQ) Clone(ctx context.Context) data.PicturesQ {
	return newPicturesQ(nrcontext.SetTxnToGorm(ctx, q.db))
}

func (q *picturesQ) Transaction(fn func(tx data.PicturesQ) error) error {
	return q.db.Transaction(func(tx *gorm.DB) error {
		return fn(newPicturesQ(tx))
	})
}

func (q *picturesQ) Create(picture *data.Picture) (int64, error) {
	if err := q.db.Create(&picture).Error; err != nil {
		return 0, err
	}
	return picture.ID, nil
}

func (q *picturesQ) Update(picture *data.Picture) error {
	return q.db.Save(picture).Error
}

func (q *picturesQ) Delete(pictureID int64) error {
	return pkgdata.ErrorFromDelete(q.db.Delete(&data.Picture{ID: pictureID}))
}

func (q *picturesQ) Select(params resources.QueryParams) (pictures []data.Picture, count int64, err error) {
	if err := q.db.Scopes(page(params)).Where(q.filters).Find(&pictures).Error; err != nil {
		return nil, 0, err
	}

	if err := q.db.Table("pictures").Where(q.filters).Count(&count).Error; err != nil {
		return nil, 0, err
	}
	return
}

func (q *picturesQ) SelectVisible(userID string, params resources.QueryParams) (pictures []data.Picture, count int64, err error) {
	if err := q.visiblePictures(userID).
		Scopes(page(params)).
		Find(&pictures).Error; err != nil {
		return nil, 0, err
	}

	if err := q.visiblePictures(userID).Count(&count).Error; err != nil {
		return nil, 0, err
	}

	return
}

func (q *picturesQ) IsLiked(userID string, pictureID int64) (bool, error) {
	var count int64
	if err := q.db.
		Table("users_liked_pics").
		Where("user_id = ? AND picture_id = ?", userID, pictureID).
		Count(&count).Error; err != nil {
		return false, err
	}

	if count == 0 {
		return false, nil
	}
	return true, nil
}

func (q *picturesQ) Like(relation data.UsersLikedPics) error {
	return pkgdata.FromGormError(q.db.Create(relation).Error)
}

func (q *picturesQ) Unlike(relation data.UsersLikedPics) error {
	return pkgdata.ErrorFromDelete(q.db.Where("user_id = ? AND picture_id = ?", relation.UserID, relation.PictureID).
		Delete(relation))
}

func (q *picturesQ) GetByID(id int64) (pic *data.Picture, err error) {
	if err := q.db.Take(&pic, id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return
}

func (q *picturesQ) GetFullInfoByID(id int64) (picture *data.PictureFullInfo, err error) {
	// We can use simple JOIN instead of CROSS JOIN, but since mapping picture -> likes in query
	// is 1-to-1 latter is used to avoid unnecessary "ON" statement
	if err := q.db.
		Table("pictures").
		Select(`pictures.*, u.first_name, u.last_name, u.has_avatar, likes,
			array_agg(uvp.user_id) filter (where uvp.user_id is not null) AS shared_with`).
		Joins("JOIN users u ON pictures.owner = u.id").
		Joins("LEFT JOIN users_visible_pics uvp ON pictures.id = uvp.picture_id").
		Joins("CROSS JOIN (SELECT COUNT(1) AS likes FROM users_liked_pics WHERE picture_id = ?) t", id).
		Group("pictures.id, u.first_name, u.last_name, u.has_avatar, likes").
		Take(&picture, id).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}

	picture.OwnerDetails.ID = picture.Owner
	return
}

func (q *picturesQ) CheckViewPermission(picID int64, userID string) (bool, error) {
	var count int64
	if err := q.db.
		Table("users_visible_pics").
		Where("user_id = ? AND picture_id = ?", userID, picID).
		Count(&count).Error; err != nil {
		return false, err
	}

	if count == 0 {
		return false, nil
	}
	return true, nil
}

func (q *picturesQ) GetPermittedUsers(picID int64) (users []string, err error) {
	if err := q.db.
		Table("users_visible_pics").
		Select("user_id").
		Where("picture_id = ?", picID).
		Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

func (q *picturesQ) UpdateViewPermissions(picID int64, users []string) error {
	if err := q.db.
		Table("users_visible_pics").
		Delete(&users, "picture_id = ?", picID).Error; err != nil {
		return err
	}

	for i := range users {
		if err := q.db.
			Table("users_visible_pics").
			Create(map[string]interface{}{"picture_id": picID, "user_id": users[i]}).Error; err != nil {
			return err
		}
	}
	return nil
}

func (q *picturesQ) FilterByOwner(id string) {
	q.filters["owner"] = id
}

func (q picturesQ) visiblePictures(userID string) *gorm.DB {
	return q.db.Table("pictures").
		Joins("LEFT JOIN users_visible_pics uvp ON pictures.id = uvp.picture_id").
		Joins("LEFT JOIN users u ON u.id = pictures.owner").
		Joins("LEFT JOIN users_friends uf ON uf.user_id = u.id").
		Where("is_suspended = ? AND (privacy = ? OR uvp.user_id = ?", false, data.PicturePrivacyPublic, userID).
		Or("privacy = ? AND uf.friend_user_id = ?)", data.PicturePrivacyPublicToFriends, userID).
		Where(q.filters)
}

func page(params resources.QueryParams) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		page := params.Page
		if page <= 0 {
			page = 1
		}

		pageSize := params.Size
		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 10
		}

		offset := (page - 1) * pageSize
		return db.Offset(offset).Limit(pageSize)
	}
}
