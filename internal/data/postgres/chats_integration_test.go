//go:build integration
// +build integration

package postgres

import (
	"os"
	"testing"
	"time"

	"gitlab.com/my-group322/pictures/pics-svc/resources"

	migrate "github.com/rubenv/sql-migrate"

	"gitlab.com/my-group322/pictures/pics-svc/internal/data"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/my-group322/pictures/pics-svc/internal/config"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data/migrations"
	driver "gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	q = struct {
		UsersQ    usersQ
		PicturesQ picturesQ
		ChatsQ    chatsQ
	}{}
	notAvailableDB *gorm.DB
)

func TestMain(m *testing.M) {
	os.Exit(setup(m))
}

// This func is necessary because of os.Exit and defer conflicts
func setup(m *testing.M) int {
	log := logrus.New()

	cfg, err := config.New(log)
	if err != nil {
		panic(errors.Wrap(err, "failed to setup config"))
	}

	db, err := gorm.Open(driver.Open(cfg.DBUrl), &gorm.Config{
		SkipDefaultTransaction: true,
		Logger:                 logger.Default.LogMode(logger.Silent)},
	)
	if err != nil {
		panic(errors.Wrap(err, "failed to open db"))
	}

	rawDB, _ := db.DB()
	defer func() {
		if err = rawDB.Close(); err != nil {
			log.WithError(err).Fatalf("failed to close db")
		}
	}()

	if err = migrations.Migrate(log, db, migrate.Up); err != nil {
		panic(errors.Wrap(err, "failed to apply migrations"))
	}
	defer func() {
		if err = migrations.Migrate(log, db, migrate.Down); err != nil {
			log.WithError(err).Error("failed to migrate down")
		}
	}()

	q.UsersQ = *newUsersQ(db)
	q.PicturesQ = *newPicturesQ(db)
	q.ChatsQ = *newChatsQ(db)

	notAvailableDB, _ = gorm.Open(driver.Open(""), &gorm.Config{
		Logger: logger.Default.LogMode(logger.Silent),
	})

	return m.Run()
}

func TestChatsQ_Create(t *testing.T) {
	t.Run("positive", func(t *testing.T) {
		userID := createUser(t).ID
		pictureID := createPicture(t, userID, data.PicturePrivacyPublic, false)

		chat := &data.Chat{
			PictureID:    pictureID,
			ChattingWith: userID,
		}

		chatID, err := q.ChatsQ.Create(chat)
		require.NoError(t, err)

		chatActual, err := q.ChatsQ.GetByID(chatID)
		require.NoError(t, err)

		require.EqualValues(t, chat, chatActual)
	})

	t.Run("negative_picture_id_invalid", func(t *testing.T) {
		userID := createUser(t).ID

		_, err := q.ChatsQ.Create(&data.Chat{
			PictureID:    -1,
			ChattingWith: userID,
		})
		require.Error(t, err)
	})

	t.Run("negative_chatting_with_invalid", func(t *testing.T) {
		pictureID := createPicture(t, createUser(t).ID, data.PicturePrivacyPublic, false)

		_, err := q.ChatsQ.Create(&data.Chat{
			PictureID:    pictureID,
			ChattingWith: uuid.NewString(),
		})
		require.Error(t, err)
	})

	t.Run("negative_picture-chatting-with_conflict", func(t *testing.T) {
		userID := createUser(t).ID
		pictureID := createPicture(t, userID, data.PicturePrivacyPublic, false)

		chat := &data.Chat{
			PictureID:    pictureID,
			ChattingWith: userID,
		}

		_, err := q.ChatsQ.Create(chat)
		require.NoError(t, err)

		_, err = q.ChatsQ.Create(chat)
		require.Error(t, err)
	})
}

func TestChatsQ_Select(t *testing.T) {
	t.Run("positive", func(t *testing.T) {
		user := createUser(t)
		user1 := createUser(t)
		pictureID := createPicture(t, user.ID, data.PicturePrivacyPublic, false)

		_ = createChat(t, user.ID, pictureID)
		_ = createChat(t, user1.ID, pictureID)

		err := q.UsersQ.UpdateHasAvatar(user.ID, true)
		require.NoError(t, err)
		user.HasAvatar = true

		chats, err := q.ChatsQ.Select(pictureID)
		require.NoError(t, err)
		require.Len(t, chats, 2)

		chatsHasUser := func(user *data.User) {
			for _, chat := range chats {
				if chat.ChattingWith == user.ID {
					details := chat.ChattingWithDetails

					require.Equal(t, user.FirstName, details.FirstName)
					require.Equal(t, user.LastName, details.LastName)
					require.Equal(t, user.HasAvatar, details.HasAvatar)

					return
				}
			}
			require.FailNow(t, "chats don't contain user")
		}

		chatsHasUser(user)
		chatsHasUser(user1)
	})

	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newChatsQ(notAvailableDB).Select(0)
		require.Error(t, err)
	})
}

func TestChatsQ_GetByID(t *testing.T) {
	t.Run("positive", func(t *testing.T) {
		user := createUser(t)
		pictureID := createPicture(t, user.ID, data.PicturePrivacyPublic, false)

		chat := createChat(t, user.ID, pictureID)

		actualChat, err := q.ChatsQ.GetByID(chat.ID)
		require.NoError(t, err)
		require.EqualValues(t, chat, actualChat)
	})

	t.Run("positive_not_found", func(t *testing.T) {
		chat, err := q.ChatsQ.GetByID(-1)
		require.NoError(t, err)
		require.Nil(t, chat)
	})

	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newChatsQ(notAvailableDB).GetByID(0)
		require.Error(t, err)
	})
}

func TestChatsQ_GetChatHistory(t *testing.T) {
	t.Run("negative_dial_err", func(t *testing.T) {
		_, err := newChatsQ(notAvailableDB).GetChatHistory(0, resources.QueryParams{})
		require.Error(t, err)
	})
}

func createPicture(t *testing.T, userID string, privacy data.PicturePrivacy, isSuspended bool) int64 {
	pictureID, err := q.PicturesQ.Create(&data.Picture{
		Thumbnail:   []byte("cool thumbnail"),
		Owner:       userID,
		Privacy:     privacy,
		IsSuspended: isSuspended,
	})
	require.NoError(t, err)

	return pictureID
}

func createUser(t *testing.T) *data.User {
	user := &data.User{
		ID:        uuid.NewString(),
		Email:     uuid.NewString(),
		FirstName: "Ilya",
		LastName:  "Syvoshapka",
	}

	err := q.UsersQ.Create(user)
	require.NoError(t, err)

	return user
}

func createChat(t *testing.T, userID string, pictureID int64) *data.Chat {
	chat := &data.Chat{
		PictureID:    pictureID,
		ChattingWith: userID,
	}

	chatID, err := q.ChatsQ.Create(chat)
	require.NoError(t, err)

	err = q.ChatsQ.AddMessage(&data.Message{
		ChatID:           chatID,
		PictureOwnerSaid: false,
		CreatedAt:        time.Now(),
		Message:          "lol",
	})
	require.NoError(t, err)

	chat.ID = chatID
	return chat
}
