package postgres

import (
	"context"

	nrcontext "github.com/ilyasiv2003/newrelic-context"
	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
	"gorm.io/gorm"
)

type chatsQ struct {
	db *gorm.DB
}

func newChatsQ(db *gorm.DB) *chatsQ {
	return &chatsQ{
		db: db,
	}
}

func NewChatsQCloner(db *gorm.DB) data.ChatsQCloner {
	return newChatsQ(db)
}

func (q *chatsQ) Clone(ctx context.Context) data.ChatsQ {
	return newChatsQ(nrcontext.SetTxnToGorm(ctx, q.db))
}

func (q *chatsQ) Create(chat *data.Chat) (int64, error) {
	if err := q.db.Create(&chat).Error; err != nil {
		return 0, pkgdata.FromGormError(err)
	}
	return chat.ID, nil
}

func (q *chatsQ) AddMessage(message *data.Message) error {
	return q.db.Create(&message).Error
}

func (q *chatsQ) Select(pictureID int64) (chats []data.ChatFullInfo, err error) {
	if err := q.db.
		Table("chats").
		Select("chats.id, chats.chatting_with, u.first_name, u.last_name, u.has_avatar").
		Joins("JOIN users u ON chats.chatting_with = u.id").
		Joins("JOIN messages m ON chats.id = m.chat_id"). // filters empty chats
		Find(&chats, "picture_id = ?", pictureID).Error; err != nil {
		return nil, err
	}
	return
}

func (q *chatsQ) GetByID(chatID int64) (chat *data.Chat, err error) {
	if err := q.db.Take(&chat, chatID).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return
}

func (q *chatsQ) GetByChattingWithAndPicID(userID string, picID int64) (chat *data.Chat, err error) {
	if err := q.db.Where("picture_id = ? AND chatting_with = ?", picID, userID).Take(&chat).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return
}

func (q *chatsQ) GetChatHistory(chatID int64, params resources.QueryParams) (messages []data.Message, err error) {
	if err := q.db.
		Scopes(cursor(params, "messages")).
		Table("messages").
		Joins("JOIN chats c ON c.id = messages.chat_id").
		Order("messages.id desc").
		Find(&messages, "c.id = ?", chatID).Error; err != nil {
		return nil, err
	}
	return
}

func cursor(params resources.QueryParams, paginatedTable string) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		before := params.Before
		if before < 0 {
			before = 0
		}

		pageSize := params.Size
		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 10
		}

		return db.Where(paginatedTable+".id < ?", before).Limit(pageSize)
	}
}
