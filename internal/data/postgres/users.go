package postgres

import (
	"context"

	nrcontext "github.com/ilyasiv2003/newrelic-context"

	"gitlab.com/my-group322/pictures/auth-svc/pkg/pkgdata"
	"gitlab.com/my-group322/pictures/pics-svc/internal/data"
	"gitlab.com/my-group322/pictures/pics-svc/resources"
	"gorm.io/gorm"
)

type usersQ struct {
	db *gorm.DB
}

func newUsersQ(db *gorm.DB) *usersQ {
	return &usersQ{
		db: db,
	}
}

func NewUsersQCloner(db *gorm.DB) data.UsersQCloner {
	return newUsersQ(db)
}

func (q *usersQ) Clone(ctx context.Context) data.UsersQ {
	return newUsersQ(nrcontext.SetTxnToGorm(ctx, q.db))
}

func (q *usersQ) Create(user *data.User) error {
	return pkgdata.FromGormError(q.db.Create(user).Error)
}

func (q *usersQ) Update(user *data.User) error {
	return q.db.Save(user).Error
}

func (q *usersQ) Select(params resources.QueryParams) (users []data.User, err error) {
	if err := q.db.Scopes(page(params)).Find(&users).Error; err != nil {
		return nil, err
	}
	return
}

func (q *usersQ) SelectWithoutLimit() (users []data.User, err error) {
	if err := q.db.Find(&users).Error; err != nil {
		return nil, err
	}
	return
}

func (q *usersQ) GetByID(id string) (user *data.User, err error) {
	var u data.User
	if err := q.db.Where("id = ?", id).Take(&u).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		}
		return nil, err
	}
	return &u, err
}

func (q *usersQ) AddFriend(relation data.UsersFriends) error {
	return pkgdata.FromGormError(q.db.Create(relation).Error)
}

func (q *usersQ) DeleteFriend(relation data.UsersFriends) error {
	return pkgdata.ErrorFromDelete(q.db.Where("user_id = ? AND friend_user_id = ?", relation.UserID, relation.FriendUserID).
		Delete(relation))
}

func (q *usersQ) IsFriend(userID, friendID string) (bool, error) {
	var count int64
	if err := q.db.
		Table("users_friends").
		Where("user_id = ? AND friend_user_id = ?", userID, friendID).
		Count(&count).Error; err != nil {
		return false, err
	}

	if count == 0 {
		return false, nil
	}
	return true, nil
}

func (q *usersQ) UpdateHasAvatar(id string, hasAvatar bool) error {
	return q.db.
		Model(data.User{}).
		Where("id = ?", id).
		Updates(data.User{HasAvatar: hasAvatar}).Error
}

func (q *usersQ) FilterByFirstName(name string) {
	q.db = q.db.Where("first_name ILIKE ?", "%"+name+"%")
}

func (q *usersQ) FilterByLastName(name string) {
	q.db = q.db.Where("last_name ILIKE ?", "%"+name+"%")
}

func (q *usersQ) FilterByAnyName(name string) {
	q.db = q.db.Where("first_name ILIKE ? OR last_name ILIKE ?", "%"+name+"%", "%"+name+"%")
}

func (q *usersQ) FilterFriends(userID string) {
	q.db = q.db.
		Table("users").
		Joins("JOIN users_friends uf ON users.id = uf.friend_user_id").
		Where("user_id = ?", userID)
}

func (q *usersQ) FilterByID(ids []string) {
	q.db = q.db.Where("id IN ?", ids)
}
