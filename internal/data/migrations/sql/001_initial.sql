-- +migrate Up

create extension pg_trgm;

create table users (
    id uuid primary key,
    email varchar(320) unique not null,
    first_name varchar(30) not null,
    last_name varchar(30) not null,
    has_avatar boolean default false
);

create index users_first_name_idx on users using gin (first_name gin_trgm_ops);
create index users_last_name_idx on users using gin (last_name gin_trgm_ops);

create type picture_privacy as enum ('PUBLIC', 'TO_FRIENDS', 'TO_SELECTED');

create table pictures (
    id bigserial primary key,
    thumbnail bytea not null,
    created_at date not null,
    owner uuid not null references users(id),
    privacy picture_privacy,
    comment text default '',
    is_suspended boolean default false
);

create table users_visible_pics (
    user_id uuid not null references users(id) on delete cascade,
    picture_id bigserial not null references pictures(id) on delete cascade,
    unique (user_id, picture_id)
);

create table users_friends (
   user_id uuid not null references users(id) on delete cascade,
   friend_user_id uuid not null references users(id) on delete cascade,
   unique (user_id, friend_user_id)
);

create table chats (
    id bigserial primary key,
    picture_id bigserial not null references pictures(id) on delete cascade,
    chatting_with uuid not null references users(id) on delete cascade,
    unique (picture_id, chatting_with)
);

create table messages (
    id bigserial primary key,
    chat_id bigserial not null references chats(id) on delete cascade,
    picture_owner_said bool not null,
    created_at timestamp not null,
    message text not null
);

create table users_liked_pics (
    user_id uuid not null references users(id) on delete cascade,
    picture_id bigserial not null references pictures(id) on delete cascade,
    unique (user_id, picture_id)
);

-- +migrate Down

drop extension pg_trgm cascade;
drop type picture_privacy cascade;
drop table pictures cascade;
drop table users cascade;
drop table users_visible_pics cascade;
drop table users_friends cascade;
drop table chats cascade;
drop table messages cascade;
drop table users_liked_pics cascade;

