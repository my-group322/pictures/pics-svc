FROM golang:1.16-alpine

WORKDIR /go/src/my-group322/pictures/pics-svc
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -o /usr/local/bin/pics-svc gitlab.com/my-group322/pictures/pics-svc

FROM alpine:3.9

COPY --from=0 /usr/local/bin/pics-svc /usr/local/bin/pics-svc

ENTRYPOINT ["pics-svc"]
