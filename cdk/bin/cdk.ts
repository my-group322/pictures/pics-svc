#!/usr/bin/env node
import 'source-map-support/register'
import * as cdk from '@aws-cdk/core'
import { CdkStack } from '../lib/cdk-stack'

const app = new cdk.App()
const props = app.node.tryGetContext('default')

new CdkStack(app, 'pics-svc', props, {
  env: { account: props['account'], region: props['region'] },
  stackName: 'pics-svc',
})
