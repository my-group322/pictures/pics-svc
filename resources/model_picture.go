package resources

import (
	"time"

	jares "gitlab.com/my-group322/json-api-shared"
)

type Picture struct {
	jares.Key
	Attributes    PictureAttributes    `json:"attributes"`
	Relationships PictureRelationships `json:"relationships"`
}

type PictureAttributes struct {
	Link        string    `json:"link"`
	CreatedAt   time.Time `json:"created_at"`
	Privacy     int64     `json:"privacy"`
	Likes       int64     `json:"likes"`
	IsLiked     bool      `json:"is_liked"`
	Comment     string    `json:"comment"`
	IsSuspended bool      `json:"is_suspended"`
}

type PictureRelationships struct {
	Owner      jares.Relation           `json:"owner"`
	SharedWith jares.RelationCollection `json:"shared_with"`
	Chat       jares.Relation           `json:"chat"`
}

type PictureResponse struct {
	Data     Picture        `json:"data"`
	Included jares.Included `json:"included"`
}

type PictureListResponse struct {
	Data []Picture `json:"data"`
}
