package resources

import jares "gitlab.com/my-group322/json-api-shared"

type Account struct {
	jares.Key
	Attributes AccountAttributes `json:"attributes"`
}

type AccountAttributes struct {
	LastPasswordChange *int64 `json:"last_password_change"`
	IsVerified         bool   `json:"is_verified"`
}

type AccountResponse struct {
	Data     Account        `json:"data"`
	Included jares.Included `json:"included"`
}
