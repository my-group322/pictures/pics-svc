package resources

import jares "gitlab.com/my-group322/json-api-shared"

const (
	ResourceTypeUser    jares.ResourceType = "user"
	ResourceTypePicture jares.ResourceType = "picture"
	ResourceTypeChat    jares.ResourceType = "chat"
	ResourceTypeMessage jares.ResourceType = "message"
)
