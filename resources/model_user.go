package resources

import jares "gitlab.com/my-group322/json-api-shared"

type User struct {
	jares.Key
	Attributes    UserAttributes    `json:"attributes"`
	Relationships UserRelationships `json:"relationships"`
}

type UserAttributes struct {
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	HasAvatar bool   `json:"has_avatar"`
}

type UserRelationships struct {
	Friends jares.RelationCollection `json:"friends"`
}

type UserResponse struct {
	Data     User           `json:"data"`
	Included jares.Included `json:"included"`
}

type UserListResponse struct {
	Data []User `json:"data"`
}
