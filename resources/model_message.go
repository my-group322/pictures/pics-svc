package resources

import (
	"time"

	jares "gitlab.com/my-group322/json-api-shared"
)

type Message struct {
	jares.Key
	Attributes MessageAttributes `json:"attributes"`
}

type MessageAttributes struct {
	Message          string    `json:"message"`
	CreatedAt        time.Time `json:"created_at"`
	PictureOwnerSaid bool      `json:"picture_owner_said"`
}

type MessageListResponse struct {
	Data []Message `json:"data"`
}
