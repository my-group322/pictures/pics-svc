package resources

type SendMessage struct {
	ChatID  int64  `json:"chat_id"`
	Message string `json:"message"`
}
