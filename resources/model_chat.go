package resources

import jares "gitlab.com/my-group322/json-api-shared"

type Chat struct {
	jares.Key
	Relationships ChatRelationships `json:"relationships"`
}

type ChatRelationships struct {
	ChattingWith jares.Relation `json:"chatting_with"`
}

type ChatResponse struct {
	Data     Chat           `json:"data"`
	Included jares.Included `json:"included"`
}

type ChatListResponse struct {
	Data     []Chat         `json:"data"`
	Included jares.Included `json:"included"`
}
