package resources

type QueryParams struct {
	Page              int
	Before            int64
	Size              int
	FilterByOwner     string `schema:"filter[owner]"`
	FilterByAllNames  string `schema:"filter[all_names]"`
	FilterByFirstName string `schema:"filter[first_name]"`
	FilterByLastName  string `schema:"filter[last_name]"`
	FilterFriends     bool   `schema:"filter[friends]"`
	FilterByPicture   int64  `schema:"filter[picture]"`
}
