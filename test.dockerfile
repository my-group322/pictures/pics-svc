FROM golang:1.16.13

ENV APP_PATH=/go/src/my-group322/pictures/pics-svc

WORKDIR $APP_PATH
COPY . .

# Testing, removing all mocks lines from coverage file, so that go tool cover doesn't count them
CMD go test -v -timeout 30s -coverpkg=./... -race -coverprofile=coverage.tmp.out \
 --tags=functional,integration $(go list ./... | grep -v '/vendor/') && \
 cat coverage.tmp.out | grep -v "mocks" > coverage.out && \
 go tool cover -func coverage.out
